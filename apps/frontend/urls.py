from django.conf.urls import url
from frontend.views import pages

urlpatterns = [
    url(r'^$', pages.index, name="index"),
]
