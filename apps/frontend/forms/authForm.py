from django import forms


class Auth(forms.Form):
    email = forms.EmailField(max_length=255, widget=forms.TextInput(attrs={'placeholder': 'E-mail', 'class': 'form-control text1', 'name': 'login_email'}))
    password = forms.CharField(widget=forms.PasswordInput(attrs={'placeholder': 'Password', 'class': 'form-control text2', 'name': 'login_password'}))
    remember_me = forms.CharField(required=False, widget=forms.CheckboxInput(attrs={'class': 'checkbox styled', 'required': 'false'}))


class Register(forms.Form):
    email = forms.CharField(max_length=255, widget=forms.TextInput(attrs={'placeholder': 'E-mail', 'class': 'form-control text-lowercase', 'name': 'email'}), label="Enter your E-mail:")
    name = forms.CharField(max_length=255, widget=forms.TextInput(attrs={'placeholder': 'Name', 'class': 'form-control', 'name': 'name'}), label="Enter your name:")
    forum_username = forms.CharField(max_length=255, widget=forms.TextInput(attrs={'placeholder': 'Support Forum Username', 'class': 'form-control', 'name': 'user_name'}), label="Enter your prefered forum username:")
    password = forms.CharField(widget=forms.PasswordInput(attrs={'placeholder': 'Password', 'class': 'form-control', 'name': 'password'}), label="Enter your password:", min_length=8)
    repeat_password = forms.CharField(widget=forms.PasswordInput(attrs={'placeholder': 'Repeat Password', 'class': 'form-control', 'name': 'repeat_password'}), label="Repeat your password:", min_length=8)
    promo_code = forms.CharField(widget=forms.PasswordInput(attrs={'placeholder': 'Enter Promo Code (Optional)', 'class': 'form-control promo-register', 'name': 'promo_code'}), label="Promo Code:", required=False)
    agreement = forms.CharField(widget=forms.CheckboxInput(attrs={'class': 'required checkbox styled'}))


class RegisterInvite(forms.Form):
    email = forms.CharField(max_length=255, widget=forms.TextInput(attrs={'placeholder': 'E-mail', 'class': 'form-control text-lowercase', 'name': 'email'}))
    name = forms.CharField(max_length=255, widget=forms.TextInput(attrs={'placeholder': 'Name', 'class': 'form-control', 'name': 'name'}), label="Enter your name:")
    forum_username = forms.CharField(max_length=255, widget=forms.TextInput(attrs={'placeholder': 'Support Forum Username', 'class': 'form-control', 'name': 'user_name'}), label="Enter your prefered forum username:")
    password = forms.CharField(widget=forms.PasswordInput(attrs={'placeholder': 'Password', 'class': 'form-control', 'name': 'password'}), label="Enter your password:", min_length=8)
    repeat_password = forms.CharField(widget=forms.PasswordInput(attrs={'placeholder': 'Repeat Password', 'class': 'form-control', 'name': 'repeat_password'}), label="Repeat your password:", min_length=8)
    agreement = forms.CharField(widget=forms.CheckboxInput(attrs={'class': 'required checkbox styled'}))


class Recovery(forms.Form):
    email = forms.CharField(max_length=255, widget=forms.TextInput(attrs={'placeholder': 'E-mail', 'class': 'form-control text-lowercase', 'name': 'email'}), label="Enter your E-mail:")


class Passwords(forms.Form):
    email = forms.CharField(max_length=255, widget=forms.TextInput(attrs={'placeholder': 'E-mail', 'class': 'form-control text-lowercase', 'name': 'email'}), label="Enter your E-mail:")
    password = forms.CharField(widget=forms.PasswordInput(attrs={'placeholder': 'Password', 'class': 'form-control', 'name': 'password'}), label="Enter new password:")
    repeat_password = forms.CharField(widget=forms.PasswordInput(attrs={'placeholder': 'Repeat New Password', 'class': 'form-control', 'name': 'repeat_password'}), label="Repeat new password:")


class CheckAvailability(forms.Form):
    forum_username = forms.CharField(max_length=255, widget=forms.TextInput(attrs={'placeholder': 'Support Forum Username', 'class': 'form-control', 'name': 'user_name'}), label="Enter your prefered forum username:")


class Resend(forms.Form):
    email = forms.CharField(max_length=255, widget=forms.TextInput(attrs={'placeholder': 'E-mail', 'class': 'form-control text-lowercase', 'name': 'email'}), label="Enter your E-mail:")
