from __future__ import absolute_import, unicode_literals
from blomp.celery import app
from frontend.models import models
from utils.openstack.swift_client import SwiftClient as swift_client


@app.task
def move_files(promo_code, container):
    swift = swift_client()
    promo = models.PromoCodes()
    promo_files = models.PromoUploads()
    promo_exists = promo.promo_exists(promo_code)
    if promo_exists:
        # For future reference - check if promo is expired
        # Get promo files
        files = promo_files.get_files(promo_code)
        for f in files:
            copy = swift.copy_object('promoUeoibVsawM', f.random_name, container, f.file_name)
            if copy:
                # Delete the object
                swift.delete_object('promoUeoibVsawM', f.random_name)

        # Set promo expired
        promo.set_expired(promo_code)
    else:
        return False
