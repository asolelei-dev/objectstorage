from __future__ import unicode_literals
from django.db import models
from django.core.exceptions import ObjectDoesNotExist
from argon2 import PasswordHasher
from argon2.exceptions import VerifyMismatchError


class Billing(models.Model):
    client_id = models.IntegerField()
    email = models.CharField(max_length=45, default="demo@blomp.com")
    plan = models.CharField(max_length=45, default="free")
    billing_cycle = models.CharField(max_length=10, null=True)
    billing_date = models.CharField(max_length=32, null=True)

    class Meta:
        managed = False
        db_table = 'billing'

    def get_client_billing(self, client_id):
        try:
            billing = Billing.objects.filter(client_id=client_id)
            return billing

        except ObjectDoesNotExist:
            return None

        except IndexError:
            return False

    def update_billing(self, client_id):
        try:
            billing_plan = Billing.objects.get(client_id=client_id)
            current_plan = billing_plan.plan
            if current_plan == 'basic':
                new_size = '40 GB'
                billing_plan.plan = new_size
                billing_plan.save()
                return True
            else:
                size_value = current_plan.split(' ')
                new_size = int(size_value[0]) + 20
                billing_plan.plan = '{} GB'.format(str(new_size))
                billing_plan.save()
                return True

        except ObjectDoesNotExist:
            return None
        except IndexError:
            return None

    def save_billing(self, client_id, email, plan, billing_cycle, billing_date):
        try:
            billing = Billing(client_id=client_id, email=email, plan=plan, billing_cycle=billing_cycle, billing_date=billing_date)
            billing.save()

        except ObjectDoesNotExist:
            return None
        except IndexError:
            return None

    def deleteBilling(self, client_id):
        try:
            Billing.objects.filter(id=client_id).delete()

        except ObjectDoesNotExist:
            return None
        except IndexError:
            return None


class ClientLogins(models.Model):
    client_id = models.IntegerField()
    last_login = models.CharField(max_length=45)
    ip = models.CharField(max_length=16)

    class Meta:
        managed = False
        db_table = 'client_logins'

    def saveLogin(self, client_id, last_login, ip):
        try:
            savelogin = ClientLogins(client_id=client_id, last_login=last_login, ip=ip)
            savelogin.save()
            return True

        except ObjectDoesNotExist:
            return False

        except IndexError:
            return False


class Clients(models.Model):
    os_user_id = models.CharField(max_length=128, blank=True, null=True)
    email = models.CharField(max_length=128)
    password = models.CharField(max_length=512)
    name = models.CharField(max_length=512, blank=True, null=True)
    address = models.CharField(max_length=512, blank=True, null=True)
    phone = models.CharField(max_length=48, blank=True, null=True)
    company = models.CharField(max_length=256, blank=True, null=True)
    fb_user_id = models.CharField(max_length=128)
    forum_username = models.CharField(max_length=64)
    regdate = models.CharField(max_length=45)
    status = models.IntegerField(blank=True, null=True)
    deleted = models.IntegerField(blank=True, null=True)
    confirm_number = models.CharField(max_length=16, blank=True, null=True)
    confirmed = models.IntegerField(blank=True, null=True)
    first_payment = models.IntegerField(blank=True, null=True)
    check_confirmed = models.CharField(max_length=16, blank=True, null=True)
    resetnum = models.CharField(max_length=256, blank=True, null=True)
    password_reset = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'clients'

    def authenticate(self, email, password):
        if Clients.objects.filter(email=email).exists():
            try:
                ph = PasswordHasher()
                client = Clients.objects.filter(email=email).values()
                client_id = client[0]['id']
                client_password = client[0]['password']
                try:
                    verify = ph.verify(client_password, password)
                    return client_id

                except VerifyMismatchError:
                    return None

            except ObjectDoesNotExist:
                return None

            except IndexError:
                return None
        else:
            return False

    def getClientDetails(self, client_id):
        try:
            client = Clients.objects.get(id=client_id)
            return client

        except ObjectDoesNotExist:
            return None
        except IndexError:
            return False

    def getClientDetailsEmail(self, email):
        try:
            client = Clients.objects.get(email=email)
            return client

        except ObjectDoesNotExist:
            return False
        except IndexError:
            return False

    def email_exists(self, email):
        try:
            email = Clients.objects.filter(email=email)
            if len(email) > 0:
                return True
            else:
                return False

        except ObjectDoesNotExist:
            return None
        except IndexError:
            return None

    def get_email_details(self, email):
        try:
            client = Clients.objects.get(email=email)
            return client

        except ObjectDoesNotExist:
            return None
        except IndexError:
            return False

    def save_client(self, os_user_id, email, password, name, forum_username, regdate, status, deleted, confirm_number, confirmed, first_payment, plan):
        billing = Billing()
        try:
            client = Clients(os_user_id=os_user_id, email=email, password=password, name=name, forum_username=forum_username, regdate=regdate, status=status, deleted=deleted, confirm_number=confirm_number, confirmed=confirmed, first_payment=first_payment)
            client.id
            client.save()
            billing.save_billing(client_id=client.id, email=email, plan=plan, billing_cycle="monthly", billing_date=regdate)
            return client.id

        except ObjectDoesNotExist:
            return None
        except IndexError:
            return None

    def updateOSclientID(self, client_id, ClientID):
        try:
            client = Clients.objects.get(pk=client_id)
            client.os_user_id = ClientID
            client.save()
            return True

        except ObjectDoesNotExist:
            return None
        except IndexError:
            return None

    def setClientRecovery(self, email, recoveryNumber):
        try:
            client = Clients.objects.get(email=email)
            client.resetnum = recoveryNumber
            client.save()
            return True

        except ObjectDoesNotExist:
            return None
        except IndexError:
            return None

    def updatePassword(self, email, password):
        try:
            client = Clients.objects.get(email=email)
            client.password = password
            client.save()
            return True

        except ObjectDoesNotExist:
            return None
        except IndexError:
            return None

    def delete_client(self, client_id):
        try:
            Clients.objects.filter(id=client_id).delete()

        except ObjectDoesNotExist:
            return None
        except IndexError:
            return None

    def check_confirm(self, confirm_number):
        try:
            client = Clients.objects.get(confirm_number=confirm_number)
            return client

        except ObjectDoesNotExist:
            return None
        except IndexError:
            return False

    def checkRecovery(self, recoveryNumber):
        try:
            Clients.objects.get(resetnum=recoveryNumber)
            return True

        except ObjectDoesNotExist:
            return None
        except IndexError:
            return False

    def resetRecovery(self, email):
        try:
            client = Clients.objects.get(email=email)
            client.resetnum = ""
            client.save()
            return True

        except ObjectDoesNotExist:
            return None
        except IndexError:
            return None

    def confirmClient(self, client_id):
        try:
            client = Clients.objects.get(pk=client_id)
            client.confirmed = 1
            client.save()
            client.check_confirm = 1
            client.save()

            return True

        except IndexError:
            return None

        except ObjectDoesNotExist:
            return None


class ClientsPp(models.Model):
    client_id = models.IntegerField()
    receiver_id = models.CharField(max_length=256, blank=True, null=True)
    residence_country = models.CharField(max_length=64, blank=True, null=True)
    first_name = models.CharField(max_length=256, blank=True, null=True)
    protection_eligibility = models.CharField(max_length=256, blank=True, null=True)
    txn_id = models.CharField(max_length=256, blank=True, null=True)
    charset = models.CharField(max_length=32, blank=True, null=True)
    payment_status = models.CharField(max_length=256, blank=True, null=True)
    verify_sign = models.CharField(max_length=256, blank=True, null=True)
    business = models.CharField(max_length=256, blank=True, null=True)
    item_name = models.CharField(max_length=128, blank=True, null=True)
    mc_gross = models.CharField(max_length=16, blank=True, null=True)
    receiver_email = models.CharField(max_length=256, blank=True, null=True)
    subscr_id = models.CharField(max_length=256, blank=True, null=True)
    last_name = models.CharField(max_length=256, blank=True, null=True)
    payer_email = models.CharField(max_length=256, blank=True, null=True)
    contact_phone = models.CharField(max_length=64, blank=True, null=True)
    txn_type = models.CharField(max_length=256, blank=True, null=True)
    payment_type = models.CharField(max_length=64, blank=True, null=True)
    payment_gross = models.CharField(max_length=16, blank=True, null=True)
    ipn_track_id = models.CharField(max_length=256, blank=True, null=True)
    payer_id = models.CharField(max_length=256, blank=True, null=True)
    mc_fee = models.CharField(max_length=16, blank=True, null=True)
    payer_status = models.CharField(max_length=64, blank=True, null=True)
    custom = models.CharField(max_length=16, blank=True, null=True)
    mc_currency = models.CharField(max_length=64, blank=True, null=True)
    payment_date = models.CharField(max_length=64, blank=True, null=True)
    payment_fee = models.CharField(max_length=16, blank=True, null=True)
    transaction_subject = models.CharField(max_length=256, blank=True, null=True)
    notify_version = models.CharField(max_length=32, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'clients_pp'

    def get_user_payments(self, client_id):
        try:
            payments = ClientsPp.objects.filter(id=client_id).last()
            return payments

        except ObjectDoesNotExist:
            return False

        except IndexError:
            return False

    def transaction_exists(self, client_id, payment_date):
        try:
            transaction = ClientsPp.objects.filter(client_id=client_id).filter(payment_date=payment_date)
            if transaction.count() > 0:
                return True
            else:
                return False

        except ObjectDoesNotExist:
            return False

        except IndexError:
            return None

    def save_client_pp(self, client_id, receiver_id, residence_country, first_name,
                       protection_eligibility,
                       txn_id,
                       charset,
                       payment_status,
                       verify_sign,
                       business,
                       item_name,
                       mc_gross,
                       receiver_email,
                       subscr_id,
                       last_name,
                       payer_email,
                       contact_phone,
                       txn_type,
                       payment_type,
                       payment_gross,
                       ipn_track_id,
                       payer_id,
                       mc_fee,
                       payer_status,
                       custom,
                       mc_currency,
                       payment_date,
                       payment_fee,
                       transaction_subject,
                       notify_version):

        try:
            client_pp = ClientsPp(client_id=client_id, receiver_id=receiver_id, residence_country=residence_country,
                                  first_name=first_name,
                                  protection_eligibility=protection_eligibility,
                                  txn_id=txn_id,
                                  charset=charset,
                                  payment_status=payment_status,
                                  verify_sign=verify_sign,
                                  business=business,
                                  item_name=item_name,
                                  mc_gross=mc_gross,
                                  receiver_email=receiver_email,
                                  subscr_id=subscr_id,
                                  last_name=last_name,
                                  payer_email=payer_email,
                                  contact_phone=contact_phone,
                                  txn_type=txn_type,
                                  payment_type=payment_type,
                                  payment_gross=payment_gross,
                                  ipn_track_id=ipn_track_id,
                                  payer_id=payer_id,
                                  mc_fee=mc_fee,
                                  payer_status=payer_status,
                                  custom=custom,
                                  mc_currency=mc_currency,
                                  payment_date=payment_date,
                                  payment_fee=payment_fee,
                                  transaction_subject=transaction_subject,
                                  notify_version=notify_version)
            client_pp.save()
            return True

        except Exception:
            return False


class ClientStorage(models.Model):
    client_id = models.IntegerField()
    username = models.CharField(max_length=128, default="None")
    password = models.CharField(max_length=256, default="password")
    ec2_access_key = models.CharField(max_length=128, default="key")
    ec2_secret_key = models.CharField(max_length=128, default="key")
    container = models.CharField(max_length=256)
    size = models.CharField(max_length=32)

    class Meta:
        managed = False
        db_table = 'client_storage'

    def getStorageDetails(self, client_id):
        try:
            storage = ClientStorage.objects.get(id=client_id)
            return storage

        except ObjectDoesNotExist:
            return None

        except IndexError:
            return False

    def saveStorageDetails(self, client_id, container, size, ec2_access_key, ec2_secret_key, password, username):
        try:
            ph = PasswordHasher()
            pwd = ph.hash(password)
            storage = ClientStorage(client_id=client_id, container=container, size=size, ec2_access_key=ec2_access_key, ec2_secret_key=ec2_secret_key, password=pwd, username=username)
            storage.save()
            return True

        except ObjectDoesNotExist:
            return None

        except IndexError:
            return None


class Plans(models.Model):
    plan = models.CharField(max_length=64, blank=True, null=True)
    price_month = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    price_year = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    size = models.IntegerField(blank=True, null=True)
    description = models.TextField()

    class Meta:
        managed = False
        db_table = 'plans'

    def get_plan_details(self, plan):
        try:
            plan = Plans.objects.get(plan=plan)
            return plan

        except ObjectDoesNotExist:
            return None

        except IndexError:
            return False


class TempData(models.Model):
    client_id = models.IntegerField()
    password = models.CharField(max_length=512)
    plan = models.CharField(max_length=45)
    bucket = models.CharField(max_length=64)
    forum_username = models.CharField(max_length=512)
    promo_code = models.CharField(max_length=24, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'temp_data'

    def save_data(self, client_id, password, plan, bucket, forum_username, promo_code):
        try:
            data = TempData(client_id=client_id, password=password, plan=plan, bucket=bucket, forum_username=forum_username, promo_code=promo_code)
            data.save()
            return data.id

        except ObjectDoesNotExist:
            return None

        except IndexError:
            return None

    def getTempData(self, client_id):
        try:
            temp = TempData.objects.get(client_id=client_id)
            return temp

        except ObjectDoesNotExist:
            return None

        except IndexError:
            return False

    def deleteTempData(self, client_id):
        try:
            TempData.objects.filter(client_id=client_id).delete()

        except ObjectDoesNotExist:
            return None

        except IndexError:
            return None


class Invitations(models.Model):
    client_id = models.IntegerField()
    client_email = models.CharField(max_length=256)
    invite_email = models.CharField(max_length=256)
    invite_number = models.CharField(max_length=64, blank=True, null=True)
    invite_date = models.CharField(max_length=256)
    confirmed = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'invitations'

    def check_invite(self, invite_number):
        try:
            invite = Invitations.objects.get(invite_number=invite_number)
            if not invite.confirmed:
                return True

        except ObjectDoesNotExist:
            return False

        except IndexError:
            return False

    def check_email(self, client_email):
        try:
            check = Invitations.objects.get(invite_email=client_email)
            if not check.confirmed:
                return True

        except ObjectDoesNotExist:
            return None

        except IndexError:
            return False

    def get_invitee_email(self, client_email):
        try:
            invitation = Invitations.objects.get(invite_email=client_email)
            return invitation.client_email

        except ObjectDoesNotExist:
            return None

        except IndexError:
            return False

    def set_invitation_expired(self, client_email):
        try:
            invitation = Invitations.objects.get(invite_email=client_email)
            invitation.confirmed = True
            invitation.save()
            return True

        except IndexError:
            return None

        except Exception:
            return False

    def get_invite_email(self, invite_number):
        try:
            invitation = Invitations.objects.get(invite_number=invite_number)
            if not invitation.confirmed:
                return invitation.invite_email
            else:
                return False

        except IndexError:
            return False

        except Exception:
            return False


class Openstack(models.Model):
    domain = models.CharField(max_length=45)
    username = models.CharField(max_length=64, blank=True, null=True)
    user_id = models.CharField(max_length=128, null=True)
    password = models.CharField(max_length=45)
    project_id = models.CharField(max_length=128, null=True)
    group = models.CharField(max_length=45, null=True)
    group_id = models.CharField(max_length=128, null=True)
    role = models.CharField(max_length=45, null=True)
    role_id = models.CharField(max_length=128, null=True)
    identity_api = models.CharField(max_length=45)
    identity_api_v2 = models.CharField(max_length=45, blank=True, null=True)
    network_api = models.CharField(max_length=45)
    telemetry_api = models.CharField(max_length=45, blank=True, null=True)
    storage_api = models.CharField(max_length=45, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'openstack'

    def details(self):
        try:
            details = Openstack.objects.get()
            return details

        except ObjectDoesNotExist:
            return False

        except IndexError:
            return False


class PromoCodes(models.Model):
    name = models.CharField(max_length=512, blank=True, null=True)
    email = models.CharField(max_length=256, blank=True, null=True)
    code = models.CharField(max_length=64)
    created = models.CharField(max_length=64)
    expires = models.CharField(max_length=64)
    expired = models.IntegerField(blank=True, null=True)
    promo = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'promo_codes'

    def promo_exists(self, promo_code):
        try:
            pcode = PromoCodes.objects.filter(code=promo_code)
            if pcode.count() > 0:
                return True
            else:
                return False

        except ObjectDoesNotExist:
            return None
        except IndexError:
            return None

    def set_expired(self, promo_code):
        try:
            promo = PromoCodes.objects.get(code=promo_code)
            promo.expired = 1
            promo.save()
            return True

        except IndexError:
            return None

        except ObjectDoesNotExist:
            return None


class PromoUploads(models.Model):
    file_name = models.CharField(max_length=512)
    random_name = models.CharField(max_length=32)
    code = models.CharField(max_length=64)

    class Meta:
        managed = False
        db_table = 'promo_uploads'

    def get_files(self, promo_code):
        try:
            files = PromoUploads.objects.filter(code=promo_code)
            return files

        except ObjectDoesNotExist:
            return False

        except IndexError:
            return False
