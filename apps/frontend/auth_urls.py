from django.conf.urls import url
from frontend.views import client_auth
from frontend.views import pages
from frontend.views import data
from frontend.views import paypal_process
from frontend.views import local_ipn

urlpatterns = [
	url(r'^$', pages.index, name="index"),
	url(r'^payments/process', paypal_process.processs_ipn, name="paypal process"),
	url(r'^payments/local', local_ipn.processs_ipn, name="local paypal process"),
	url(r'^authorize', client_auth.authorize, name="client authentication"),
	url(r'^register/$', client_auth.register, name="registration"),
	url(r'^register/invitation/([\w+])', client_auth.invite, name="invitation"),
	url(r'^registration', client_auth.register_customer, name="customer registration"),
	url(r'^invitation', client_auth.register_customer_invited, name="invited customer registration"),
	url(r'^register/forum/check/', data.check_availibility, name="check forum username"),
	url(r'^confirm/([\w+])', client_auth.confirm, name="confirmation"),
	url(r'^information/$', client_auth.information, name="information"),
	url(r'^recovery/$', pages.recover, name="recover"),
	url(r'^password_recovery/$', client_auth.passwordRecovery, name="password recovery"),
	url(r'^change_password/([\w+])', client_auth.changePassword, name="reset password"),
	url(r'^passwordreset/$', client_auth.passwordReset, name="password reset"),
	url(r'^resend_verify', client_auth.resend_verify, name="resend verify"),
	url(r'^resend', client_auth.resend, name="resend"),
	url(r'^logout', client_auth.logout, name="logout"),
]