from django.shortcuts import render
from django.core.validators import EmailValidator
from django.core.exceptions import ValidationError
from frontend.models import models
from frontend.forms import authForm
from utils.openstack.identity import Identity as identitylib
from utils.openstack.swift import Swift as swiftlib
from utils.mailer.mailer import Mailer as mailer
from utils.phpbb.phpbb import Phpbb as phpbblib
from django.contrib import messages
from django.shortcuts import redirect
import json
from urllib.parse import urlparse
from django.utils.crypto import get_random_string
from argon2 import PasswordHasher
import hashlib
from django.views.decorators.csrf import csrf_exempt
from ipware import get_client_ip
import datetime
from time import time
import re
from utils.helpers import config
import requests
from frontend.tasks import move_files
from django.contrib.messages import get_messages


@csrf_exempt
def authorize(request):
    if request.POST:
        request.session.flush()
        storage = get_messages(request)
        storage.used = True
        form = authForm.Auth(request.POST)
        if form.is_valid():
            email = form.cleaned_data['email']
            password = form.cleaned_data['password']
            # Add remember me checkbox
            clients = models.Clients()
            logins = models.ClientLogins()
            details = clients.get_email_details(email)
            # Validate email
            if details is not None:
                if details.confirmed:
                    # Validate user
                    validate = clients.authenticate(email, password)
                    if validate:
                        request.session['user_id'] = validate
                        request.session.modified = True
                        if 'remember_me' not in request.POST:
                            end_session = int((time() + 60 * 60) * 1000)
                            request.session['remember'] = end_session
                            request.session.modified = True
                        else:
                            request.session['remember'] = 0
                            request.session.modified = True

                        client_ip, is_routable = get_client_ip(request)
                        if client_ip is None:
                            ip = '0.0.0.0'
                        else:
                            # We got the client's IP address
                            if is_routable:
                                ip = client_ip
                            else:
                                ip = '127.0.0.1'

                        logins.saveLogin(validate, datetime.datetime.now().strftime("%b %d %Y, %H:%M"), ip)
                        return redirect('/dashboard')

                    elif validate is None:
                        messages.add_message(request, messages.ERROR, config.AUTH_LOGIN_ERROR)
                        # Log this for login check
                        return redirect('/')

                    elif not validate:
                        return redirect('/register')

                    else:
                        messages.add_message(request, messages.ERROR, config.AUTH_LOGIN_ERROR)
                        return redirect('/')
                else:
                    messages.add_message(request, messages.ERROR, config.AUTH_CONFIRM)
                    return redirect('/')
            else:
                return redirect('/register')
        else:
            messages.add_message(request, messages.ERROR, config.AUTH_LOGIN_ERROR)
            return redirect('/')
    else:
        form = authForm.Auth()
        messages.add_message(request, messages.ERROR, config.AUTH_LOGIN_ERROR)
        return redirect('/')


def register(request):
    request.session.flush()
    storage = get_messages(request)
    storage.used = True
    form = authForm.Register()
    return render(request, 'frontend/register.html', {'form': form})


@csrf_exempt
def register_customer(request):
    if request.POST:
        request.session.flush()
        storage = get_messages(request)
        storage.used = True
        form = authForm.Register(request.POST)
        if form.is_valid():
            ph = PasswordHasher()
            mailing = mailer()
            email = form.cleaned_data['email'].lower()
            name = form.cleaned_data['name']
            forum_username = form.cleaned_data['forum_username']
            password = form.cleaned_data['password']
            repeat_password = form.cleaned_data['repeat_password']
            promo_code = form.cleaned_data['promo_code']
            agreement = form.cleaned_data['agreement']
            # Check for spam email
            # stopspam_api_request = 'http://api.stopforumspam.org/api?email={}?json'.format(email)
            # try:
            #     spam = requests.get(url=stopspam_api_request, timeout=10)
            #     if spam.status_code == 200 and spam.json()['email']['frequency'] > 2:
            #         # send email
            #         mailing.spam_detection(email)
            #         messages.add_message(request, messages.ERROR, config.AUTH_FORUM)
            #         return redirect('/register')
            #     elif spam.status_code == 404 or spam.status_code == 500 or spam.status_code == 403:
            #         mailing.spam_service_down(email, str(spam.status_code))
            #         pass
            # except requests.ConnectionError or requests.ConnectTimeout:
            #     # Send email
            #     pass

            if len(name) - name.count(' ') < 5:
                messages.add_message(request, messages.ERROR, config.AUTH_NAME)
                return redirect('/register')

            if len(forum_username) - forum_username.count(' ') < 5:
                messages.add_message(request, messages.ERROR, config.AUTH_FORUM)
                return redirect('/register')

            if not re.match("^[a-zA-Z0-9_]*$", forum_username):
                messages.add_message(request, messages.ERROR, config.AUTH_FORUM_CHAR)
                return redirect('/register')

            if agreement is None:
                messages.add_message(request, messages.ERROR, config.AUTH_TERMS)
                return redirect('/register')
            plan = 'basic'
            temp = models.TempData()
            clients = models.Clients()
            # Check if email is valid
            validator = EmailValidator()
            try:
                validator(email)
            except ValidationError:
                messages.add_message(request, messages.ERROR, config.AUTH_INVALID_EMAIL)
                return redirect('/register')

            # Check forum username
            phpbb = phpbblib()
            check_username = phpbb.check_username(forum_username)
            if not check_username:
                messages.add_message(request, messages.ERROR, config.AUTH_FORUM_USERNAME)
                return redirect('/register')

            # Check if email exists
            exists = clients.email_exists(email)
            if exists:
                messages.add_message(request, messages.ERROR, config.AUTH_EXISTING_USER)
                return redirect('/register')
            else:
                # Check password
                if password == repeat_password:
                    # Register user
                    passwd = ph.hash(password)
                    if plan == 'basic':
                        # Save user -confirm number
                        regdate = datetime.datetime.now().strftime("%B %d, %Y at %X")
                        confirm_number = get_random_string(length=16)
                        # Plan added to function to save the billing
                        clientId = clients.save_client(os_user_id=0, email=email, password=passwd, name=name,
                                                       forum_username=forum_username,
                                                       regdate=regdate,
                                                       status=1,
                                                       deleted=0,
                                                       confirm_number=confirm_number,
                                                       confirmed=0,
                                                       first_payment=0,
                                                       plan=plan)
                        # Store passwd into temp db
                        saveData = temp.save_data(client_id=clientId, password=password, plan=plan,
                                                  bucket=email,
                                                  forum_username=forum_username,
                                                  promo_code=promo_code)

                        if clientId is not None:
                            # Send email
                            forumUsername = forum_username
                            mailing.register(email, confirm_number, forumUsername)
                            messages.add_message(request, messages.SUCCESS, config.AUTH_CONFIRM_EMAIL)
                            return redirect('/register')
                        else:
                            messages.add_message(request, messages.ERROR, config.AUTH_PROCESSING_ERROR)
                            return redirect('/register')
                    else:
                        # Save user -confirm number
                        regdate = datetime.datetime.now().strftime("%B %d, %Y at %X")
                        confirm_number = get_random_string(length=16)
                        # Plan added to function to save the billing
                        clientId = clients.save_client(os_user_id=0, email=email, password=passwd, name=name,
                                                       regdate=regdate,
                                                       status=1, deleted=0,
                                                       confirm_number=confirm_number,
                                                       confirmed=0,
                                                       first_payment=0,
                                                       plan=plan)
                        # Save temp plaintext password
                        # Don't create openstack user & S3 user -> redirect to billing
                        saveData = temp.save_data(client_id=clientId, password=password, plan=plan, bucket=email, forum_username=forum_username, promo_code=promo_code)
                        if saveData is None:
                            clients.delete_client(clientId)
                            messages.add_message(request, messages.ERROR, config.AUTH_PROCESSING_ERROR)
                            return redirect('/register')
                        else:
                            # Send email
                            messages.add_message(request, messages.SUCCESS, config.AUTH_CONFIRM_ACCOUNT)
                            return redirect('/register')
                else:
                    messages.add_message(request, messages.ERROR, config.AUTH_PASSWORD_MATCH)
                    return redirect('/register')
        else:
            form = authForm.Register()
            messages.add_message(request, messages.ERROR, config.AUTH_PROCESSING_ERROR)
            return redirect('/register')
    else:
        return redirect('/')


def confirm(request, confirm_num):
    request.session.flush()
    storage = get_messages(request)
    storage.used = True
    # Get last segment of the url
    url = urlparse(request.path)
    segments = url.path.split('/')
    confirm_num = segments[-1]
    if confirm_num == "":
        messages.add_message(request, messages.ERROR, config.AUTH_PROCESSING_ERROR)
        return redirect('/information')
    # Check which user has that confirm number
    clients = models.Clients()
    billing = models.Billing()
    storage = models.ClientStorage()
    openstack = models.Openstack()
    invites = models.Invitations()
    openstackDetails = openstack.details()
    plans = models.Plans()
    temp = models.TempData()
    mailing = mailer()
    check_confirm = clients.check_confirm(confirm_num)
    client_id = check_confirm.id
    tempData = temp.getTempData(client_id)

    if check_confirm is not None:
        # Get temp password
        clientDetails = clients.getClientDetails(client_id)

        # Create blomp user
        identity = identitylib()
        token = identity.tokenv3()
        # Create openstack user and get json
        user = identity.createUser(token, clientDetails.email, clientDetails.email, tempData.password)
        userID = user['user']['id']
        # Error occured
        if not user:
            # Delete user from database and send message - delete from billing as well
            clients.delete_client(client_id)
            billing.deleteBilling(client_id)
            messages.add_message(request, messages.ERROR, config.AUTH_PROCESSING_ERROR)
            return redirect('/information')
        else:
            # Add user to group and add user role
            role = identity.addRole(token, userID)
            if not role:
                messages.add_message(request, messages.ERROR, config.AUTH_PROCESSING_ERROR)
                return redirect('/information')
            else:
                group = identity.addGroup(token, userID)
                if not group:
                    messages.add_message(request, messages.ERROR, config.AUTH_PROCESSING_ERROR)
                    return redirect('/')
            clients.updateOSclientID(client_id, userID)
            # Create container and ACL's
            swift = swiftlib()
            bucket = clientDetails.email
            createContainer = swift.createContainer(bucket)
            if createContainer:
                planDetails = plans.get_plan_details(tempData.plan)
                # Add ACL's to container
                size = swift.containerSize(bucket, planDetails.size)
                if size:
                    read = swift.containerReadACL(user['user']['id'], tempData.bucket)
                    if read:
                        write = swift.containerWriteACL(user['user']['id'], bucket)
                        if not write:
                            messages.add_message(request, messages.ERROR, config.AUTH_PROCESSING_ERROR)
                            return redirect('/information')
                    else:
                        messages.add_message(request, messages.ERROR, config.AUTH_PROCESSING_ERROR)
                        return redirect('/information')
                else:
                    messages.add_message(request, messages.ERROR, config.AUTH_PROCESSING_ERROR)
                    return redirect('/information')
            else:
                messages.add_message(request, messages.ERROR, config.AUTH_PROCESSING_ERROR)
                return redirect('/information')

        # Set user confirmed
        clients.confirmClient(client_id)

        # Store client storage info to db and create EC2 credentials

        ec2_key = hashlib.md5(clientDetails.email.encode('utf-8')).hexdigest()
        ec2_secret = hashlib.md5(tempData.password.encode('utf-8')).hexdigest()
        # Create credentials
        identity.createEC2Credentials(token, ec2_key, ec2_secret, userID, openstackDetails.project_id)

        # Get ec2 data ans store it in storage info
        ec2Request = identity.getEC2Credentials(token, userID)
        ec2Response = json.loads(ec2Request['credentials'][0]['blob'])
        # Save data
        storage.saveStorageDetails(client_id=client_id, container=clientDetails.email, size=20,
                                   ec2_access_key=ec2Response['access'],
                                   ec2_secret_key=ec2Response['secret'],
                                   password=tempData.password,
                                   username=clientDetails.email)
        # Create forum user
        phpbb = phpbblib()
        phpbb.addUser(client_id, tempData.forum_username, clientDetails.email, tempData.password)

        # Check if this is invited user and heck db for invite and if invite is valid
        # If invite is valid add storage to invitee
        check_invited = invites.check_email(clientDetails.email)

        if check_invited:
            # Get invitee email
            invitee_email = invites.get_invitee_email(clientDetails.email)
            swift.changeContainerSize(invitee_email, 20)
            # Set invitation to expired
            invites.set_invitation_expired(clientDetails.email)
            # change plan size
            change_billing = billing.update_billing(clientDetails)
            if not change_billing or change_billing is None:
                pass
            # Send email
            mailing.space_added(invitee_email)

        # Delete temp data
        temp.deleteTempData(client_id)
        mailing.confirmed(clientDetails.email)
        move_files.delay(tempData.promo_code, clientDetails.email)
        messages.add_message(request, messages.SUCCESS, config.AUTH_ACCOUNT_CONFIRMED)
        return redirect('/information')

    elif check_confirm is True:
        messages.add_message(request, messages.SUCCESS, config.AUTH_CONFIRMED_ALREADY)
        return redirect('/information')

    else:
        messages.add_message(request, messages.ERROR, config.AUTH_INVALID_CONFIRM)
        return redirect('/information')


def information(request):
    request.session.flush()
    storage = get_messages(request)
    storage.used = True
    return render(request, 'frontend/information.html')


def passwordRecovery(request):
    if request.POST:
        request.session.flush()
        storage = get_messages(request)
        storage.used = True
        form = authForm.Recovery(request.POST)
        if form.is_valid():
            email = form.cleaned_data['email']
            # Check if client exist
            clients = models.Clients()
            email_exists = clients.email_exists(email)
            if email_exists:
                # generate random number
                mailing = mailer()
                recoveryNumber = get_random_string(length=16)
                # Set recovery number
                clients.setClientRecovery(email, recoveryNumber)
                # Send email
                mailing.recovery(email, recoveryNumber)
                messages.add_message(request, messages.SUCCESS, config.AUTH_RECOVER)
                return redirect('/information')
            else:
                messages.add_message(request, messages.ERROR, config.AUTH_INVALID_EMAIL)
                return redirect('/information')


def changePassword(request, recoveryNumber):
    request.session.flush()
    storage = get_messages(request)
    storage.used = True
    # get last segment of the url
    url = urlparse(request.path)
    segments = url.path.split('/')
    recoveryNum = segments[-1]
    if recoveryNum == "":
        messages.add_message(request, messages.ERROR, config.AUTH_PROCESSING_ERROR)
        return redirect('/information')
    else:
        clients = models.Clients()
        # Check if number exists
        checkRecover = clients.checkRecovery(recoveryNum)
        if checkRecover:
            form = authForm.Passwords(request.POST)
            return render(request, 'frontend/change_password.html', {'form': form})
        else:
            messages.add_message(request, messages.ERROR, config.AUTH_PROCESSING_ERROR)
            return redirect('/information')


def passwordReset(request):
    if request.POST:
        request.session.flush()
        storage = get_messages(request)
        storage.used = True
        form = authForm.Passwords(request.POST)
        if form.is_valid():
            ph = PasswordHasher()
            email = form.cleaned_data['email']
            password = form.cleaned_data['password']
            repeatPassword = form.cleaned_data['repeat_password']
            clients = models.Clients()
            emailExists = clients.email_exists(email)
            if emailExists:
                if password == repeatPassword:
                    client_details = clients.getClientDetailsEmail(email)
                    passwd = ph.hash(password)
                    identity = identitylib()
                    token = identity.tokenv3()
                    # Check openstack ID with the database
                    user_list = identity.userList(token)
                    if not user_list:
                        messages.add_message(request, messages.ERROR, config.AUTH_PROCESSING_ERROR)
                        return redirect('/information')
                    ind = next((index for (index, d) in enumerate(user_list['users']) if d['name'] == email), None)
                    if ind is None:
                        messages.add_message(request, messages.ERROR, config.AUTH_CONFIRM_CHANGE_PASSWORD)
                        return redirect('/information')
                    else:
                        os_id = user_list['users'][int(ind)]['id']
                        if os_id == client_details.os_user_id:
                            # Change swift password
                            changePassword = identity.changeUserPassword(token, email, client_details.os_user_id, password)
                            if changePassword is True:
                                # Save new password to the database
                                clients.updatePassword(email, passwd)

                                # Reset recovery number to empty
                                clients.resetRecovery(email)
                                messages.add_message(request, messages.SUCCESS, config.AUTH_PASSWORD_UPDATE)
                                return redirect('/information')
                            else:
                                messages.add_message(request, messages.ERROR, config.AUTH_PROCESSING_ERROR)
                                return redirect('/information')
                        else:
                            # Update client ID in the database
                            clients.updateOSclientID(client_details.id, os_id)
                            # Change swift password
                            changePassword = identity.changeUserPassword(token, email, os_id, password)
                            if changePassword is True:
                                # Save new password to the database
                                clients.updatePassword(email, passwd)

                                # Reset recovery number to empty
                                clients.resetRecovery(email)
                                messages.add_message(request, messages.SUCCESS, config.AUTH_PASSWORD_UPDATE)
                                return redirect('/information')
                            else:
                                messages.add_message(request, messages.ERROR, config.AUTH_PROCESSING_ERROR)
                                return redirect('/information')

                else:
                    messages.add_message(request, messages.ERROR, config.AUTH_PASSWORD_NOMATCH)
                    return redirect('/information')
            else:
                messages.add_message(request, messages.ERROR, config.AUTH_INVALID_EMAIL)
                return redirect('/information')


def invite(request, invite_number):
    request.session.flush()
    storage = get_messages(request)
    storage.used = True
    url = urlparse(request.path)
    segments = url.path.split('/')
    invite_number = segments[-1]
    invitations = models.Invitations()
    invite_email = invitations.get_invite_email(invite_number)
    if not invite_email:
        form = authForm.Register()
    else:
        form = authForm.RegisterInvite(initial={'email': invite_email})
    return render(request, 'frontend/register_invited.html', {'form': form, 'invite_number': invite_number})


def register_customer_invited(request):
    if request.POST:
        request.session.flush()
        storage = get_messages(request)
        storage.used = True
        form = authForm.RegisterInvite(request.POST)
        if form.is_valid():
            ph = PasswordHasher()
            mailing = mailer()
            email = form.cleaned_data['email']
            name = form.cleaned_data['name']
            forum_username = form.cleaned_data['forum_username']
            password = form.cleaned_data['password']
            repeat_password = form.cleaned_data['repeat_password']
            agreement = form.cleaned_data['agreement']
            # Check for spam email
            stopspam_api_request = 'http://api.stopforumspam.org/api?email={}?json'.format(email)
            try:
                spam = requests.get(url=stopspam_api_request, timeout=10)
                if spam.status_code == 200 and spam.json()['email']['frequency'] > 2:
                    # send email
                    mailing.spam_detection(email)
                    messages.add_message(request, messages.ERROR, config.AUTH_FORUM)
                    return redirect('/register')
                elif spam.status_code == 404 or spam.status_code == 500 or spam.status_code == 403:
                    mailing.spam_service_down(email, str(spam.status_code))
                    pass
            except requests.ConnectionError or requests.ConnectTimeout:
                # Send email
                pass

            if len(name) < 5:
                messages.add_message(request, messages.ERROR, config.AUTH_NAME)
                return redirect('/register')
            if len(forum_username) < 5:
                messages.add_message(request, messages.ERROR, config.AUTH_FORUM)
                return redirect('/register')

            url = urlparse(request.path)
            segments = url.path.split('/')
            invite_number = segments[-1]
            if agreement is None:
                messages.add_message(request, messages.ERROR, config.AUTH_TERMS)
                return redirect('/register')
            plan = 'basic'
            temp = models.TempData()
            clients = models.Clients()
            invites = models.Invitations()
            # Check if email is valid
            validator = EmailValidator()
            try:
                validator(email)
            except ValidationError:
                messages.add_message(request, messages.ERROR, config.AUTH_INVALID_EMAIL)
                return redirect('/register')

            # Check if the invitation is valid in the database
            check_invite = invites.check_invite(invite_number)

            if not check_invite:
                messages.add_message(request, messages.ERROR, 'Invalid invitation code: {}'.format(invite_number))
                return redirect('/')

            # Check forum username
            phpbb = phpbblib()
            check_username = phpbb.check_username(forum_username)
            if not check_username:
                messages.add_message(request, messages.ERROR, config.AUTH_FORUM_USERNAME)
                return redirect('/register')

            # Check if email exists
            exists = clients.email_exists(email)
            if exists:
                messages.add_message(request, messages.ERROR, config.AUTH_EXISTING_USER)
                return redirect('/register')
            else:
                # Check password
                if password == repeat_password:
                    # Register user
                    passwd = ph.hash(password)
                    if plan == 'basic':
                        # Save user -confirm number
                        regdate = datetime.datetime.now().strftime("%B %d, %Y at %X")
                        confirm_number = get_random_string(length=16)
                        # Plan added to function to save the billing
                        clientId = clients.save_client(os_user_id=0, email=email, password=passwd, name=name,
                                                       forum_username=forum_username,
                                                       regdate=regdate,
                                                       status=1,
                                                       deleted=0,
                                                       confirm_number=confirm_number,
                                                       confirmed=0,
                                                       first_payment=0,
                                                       plan=plan)
                        # Store passwd into temp db
                        saveData = temp.save_data(client_id=clientId, password=password, plan=plan, bucket=email, forum_username=forum_username)

                        if clientId is not None:
                            # Send email
                            forumUsername = 'blomp_' + str(clientId)
                            mailing.register(email, confirm_number, forumUsername)
                            messages.add_message(request, messages.SUCCESS, config.AUTH_CONFIRM_EMAIL)
                            return redirect('/register')
                        else:
                            messages.add_message(request, messages.ERROR, config.AUTH_PROCESSING_ERROR)
                            return redirect('/register')
                    else:
                        # Save user -confirm number
                        regdate = datetime.datetime.now().strftime("%B %d, %Y at %X")
                        confirm_number = get_random_string(length=16)
                        # Plan added to function to save the billing
                        clientId = clients.save_client(os_user_id=0, email=email, password=passwd, name=name,
                                                       regdate=regdate,
                                                       status=1,
                                                       deleted=0,
                                                       confirm_number=confirm_number,
                                                       confirmed=0,
                                                       first_payment=0,
                                                       plan=plan)
                        # Save temp plaintext password
                        # Don't create openstack user & S3 user -> redirect to billing
                        saveData = temp.save_data(client_id=clientId, password=password, plan=plan,
                                                  bucket=email,
                                                  forum_username=forum_username)
                        if saveData is None:
                            clients.delete_client(clientId)
                            messages.add_message(request, messages.ERROR, config.AUTH_PROCESSING_ERROR)
                            return redirect('/invitation/' + invite_number)
                        else:
                            # Send email
                            messages.add_message(request, messages.SUCCESS, config.AUTH_CONFIRM_ACCOUNT)
                            return redirect('/invitation/' + invite_number)
                else:
                    messages.add_message(request, messages.ERROR, config.AUTH_PASSWORD_MATCH)
                    return redirect('/register')
        else:
            form = authForm.RegisterInvite()
            messages.add_message(request, messages.ERROR, config.AUTH_PROCESSING_ERROR)
            return redirect('/information/')
    else:
        return redirect('/')


def resend_verify(request):
    request.session.flush()
    storage = get_messages(request)
    storage.used = True
    form = authForm.Resend()
    return render(request, 'frontend/resend_verify.html', {'form': form})


def resend(request):
    if request.POST:
        request.session.flush()
        storage = get_messages(request)
        storage.used = True
        form = authForm.Resend(request.POST)
        if form.is_valid():
            mailing = mailer()
            clients = models.Clients()
            email = form.cleaned_data['email']
            client_details = clients.getClientDetailsEmail(email)

            if not client_details:
                messages.add_message(request, messages.ERROR, config.AUTH_USER_NONEXISTS)
                return redirect('/information')
            if client_details.confirmed:
                messages.add_message(request, messages.ERROR, config.AUTH_RECONFIRM_TRUE)
                return redirect('/information')
            else:
                mailing.register(client_details.email, client_details.confirm_number, client_details.forum_username)
                messages.add_message(request, messages.SUCCESS, config.AUTH_RECONFIRM)
                return redirect('/information')
        else:
            messages.add_message(request, messages.ERROR, config.AUTH_PROCESSING_ERROR)
            return redirect('/information/')

    else:
        messages.add_message(request, messages.ERROR, config.AUTH_PROCESSING_ERROR)
        return redirect('/information/')


def logout(request):
    request.session.flush()
    storage = get_messages(request)
    storage.used = True
    return redirect('/')
