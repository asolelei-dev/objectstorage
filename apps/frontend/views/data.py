from django.http import HttpResponse
import json
from django.http import Http404
from frontend.forms import authForm
from utils.phpbb.phpbb import Phpbb as phpbblib


def check_availibility(request):
    if request.is_ajax():
        if request.method == 'POST':
            form = authForm.CheckAvailability(request.POST)
            phpbb = phpbblib()
            if form.is_valid():
                forum_username = form.cleaned_data.get('forum_username')
                check_username = phpbb.check_username(forum_username)
                if check_username:
                    data = {"response": 1, "message": "Username you selected is available."}
                    return HttpResponse(json.dumps(data), content_type='application/json')
                else:
                    data = {"response": 0, "message": "Username you selected is not available."}
                    return HttpResponse(json.dumps(data), content_type='application/json')

        else:
            raise Http404

    else:
        raise Http404