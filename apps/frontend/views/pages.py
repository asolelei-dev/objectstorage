from django.shortcuts import render
from frontend.forms import authForm


def index(request):
    form = authForm.Auth()
    return render(request, 'frontend/index.html', {'form': form})


def recover(request):
    form = authForm.Recovery()
    return render(request, 'frontend/recover.html', {'form': form})
