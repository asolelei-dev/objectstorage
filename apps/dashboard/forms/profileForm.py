from django import forms


class changePassword(forms.Form):
    current_password = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control', 'name': 'current_password', 'id': 'current-password'}), min_length=8)
    new_password = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control', 'name': 'new_password', 'id': 'new-password'}), min_length=8)
    repeat_password = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control', 'name': 'repeat_password', 'id': 'repeat-password'}), min_length=8)


class updateInfo(forms.Form):
    company = forms.CharField(max_length=255, widget=forms.TextInput(attrs={'class': 'form-control', 'name': 'company', 'id': 'company'}))
    address = forms.CharField(max_length=255, widget=forms.TextInput(attrs={'class': 'form-control', 'name': 'address', 'id': 'address'}))
    phone_number = forms.CharField(max_length=255, widget=forms.TextInput(attrs={'class': 'form-control', 'name': 'phone_number', 'id': 'phone-number'}))


class SendInvites(forms.Form):
    emails = forms.CharField(max_length=255, widget=forms.TextInput(attrs={'class': 'form-control tokenfield-purple', 'name': 'email', 'id': 'emails'}))


class PromoCode(forms.Form):
    promo_code = forms.CharField(max_length=255, widget=forms.TextInput(attrs={'class': 'form-control promo-code', 'name': 'promo_code'}))