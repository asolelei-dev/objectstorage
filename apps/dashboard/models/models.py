from django.db import models
from django.core.exceptions import ObjectDoesNotExist
from argon2 import PasswordHasher
from django.db.models import Q


class AdminGroups(models.Model):
    admin_id = models.IntegerField()
    group = models.CharField(max_length=128)
    group_id = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'admin_groups'


class AdminLogins(models.Model):
    admin_id = models.IntegerField(blank=True, null=True)
    last_login = models.IntegerField(blank=True, null=True)
    ip = models.CharField(max_length=16, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'admin_logins'


class Admins(models.Model):
    email = models.CharField(max_length=45)
    password = models.CharField(max_length=512)
    name = models.CharField(max_length=45)

    class Meta:
        managed = False
        db_table = 'admins'


class Billing(models.Model):
    client_id = models.IntegerField()
    email = models.CharField(max_length=45, default="demo@blomp.com")
    plan = models.CharField(max_length=45, default="basic")
    billing_cycle = models.CharField(max_length=10, null=True)
    billing_date = models.CharField(max_length=32, null=True)

    class Meta:
        managed = False
        db_table = 'billing'

    def get_client_billing(self, client_id):
        try:
            billing = Billing.objects.get(client_id=client_id)
            return billing

        except ObjectDoesNotExist:
            return None

    def update_billing(self, client_id, plan):
        try:
            billing = Billing.objects.get(client_id=client_id)
            billing.plan = plan
            billing.save()
            return True

        except ObjectDoesNotExist:
            return None

        except IndexError:
            return None


class ClientLogins(models.Model):
    client_id = models.IntegerField()
    last_login = models.CharField(max_length=45)
    ip = models.CharField(max_length=16)

    class Meta:
        managed = False
        db_table = 'client_logins'


class ClientStorage(models.Model):
    client_id = models.IntegerField()
    username = models.CharField(max_length=128, default="None")
    password = models.CharField(max_length=256, default="password")
    ec2_access_key = models.CharField(max_length=128, default="key")
    ec2_secret_key = models.CharField(max_length=128, default="key")
    container = models.CharField(max_length=256)
    size = models.CharField(max_length=32)

    class Meta:
        managed = False
        db_table = 'client_storage'

    def getStorageDetails(self, client_id):
        try:
            storage = ClientStorage.objects.get(client_id=client_id)
            return storage

        except ObjectDoesNotExist:
            return None

        except IndexError:
            return False

    def get_storage_details(self, client_id):
        try:
            storage = ClientStorage.objects.get(id=client_id)
            return storage

        except ObjectDoesNotExist:
            return None

        except IndexError:
            return False

    def update_client_storage(self, client_id, size):
        try:
            clientStorage = ClientStorage.objects.get(client_id=client_id)
            clientStorage.size = size
            clientStorage.save()
            return True

        except ObjectDoesNotExist:
            return None

        except IndexError:
            return None


class Clients(models.Model):
    os_user_id = models.CharField(max_length=128, blank=True, null=True)
    email = models.CharField(max_length=128)
    password = models.CharField(max_length=512)
    name = models.CharField(max_length=512, blank=True, null=True)
    address = models.CharField(max_length=512, blank=True, null=True)
    phone = models.CharField(max_length=48, blank=True, null=True)
    company = models.CharField(max_length=256, blank=True, null=True)
    fb_user_id = models.CharField(max_length=128)
    forum_username = models.CharField(max_length=64)
    regdate = models.CharField(max_length=45)
    status = models.IntegerField(blank=True, null=True)
    deleted = models.IntegerField(blank=True, null=True)
    confirm_number = models.CharField(max_length=16, blank=True, null=True)
    confirmed = models.IntegerField(blank=True, null=True)
    first_payment = models.IntegerField(blank=True, null=True)
    check_confirmed = models.CharField(max_length=16, blank=True, null=True)
    resetnum = models.CharField(max_length=256, blank=True, null=True)
    password_reset = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'clients'

    def client_exists(self, client_id):
        try:
            client = Clients.objects.get(id=client_id)
            if client:
                return True
            else:
                return False

        except ObjectDoesNotExist:
            return False

        except IndexError:
            return None

    def get_client_details(self, client_id):
        try:
            client = Clients.objects.get(id=client_id)
            return client

        except ObjectDoesNotExist:
            return None

        except IndexError:
            return False

    def user_payment(self, client_id):
        try:
            payments = Clients.objects.filter(id=client_id).values()
            payment = payments[0]['first_payment']
            return payment

        except ObjectDoesNotExist:
            return None

        except IndexError:
            return None

    def checkPassword(self, client_id, password):
        try:
            ph = PasswordHasher()
            client = Clients.objects.filter(id=client_id).values()
            client_password = client[0]['password']
            verify = ph.verify(client_password, password)
            if verify:
                return True
            else:
                return False

        except IndexError:
            return None

        except ObjectDoesNotExist:
            return None

    def updatePassword(self, client_id, password):
        try:
            ph = PasswordHasher()
            client = Clients.objects.get(pk=client_id)
            passwd = ph.hash(password)
            client.password = passwd
            client.save()
            return True

        except IndexError:
            return None

        except Exception:
            return None

    def updateInfo(self, client_id, company, address, phone):
        try:
            client = Clients.objects.get(pk=client_id)
            client.company = company
            client.address = address
            client.phone = phone
            client.save()
            return True

        except IndexError:
            return None

        except ObjectDoesNotExist:
            return None

    def updateOSclientID(self, client_id, ClientID):
        try:
            client = Clients.objects.get(pk=client_id)
            client.os_user_id = ClientID
            client.save()
            return True

        except IndexError:
            return None

        except ObjectDoesNotExist:
            return None

    def email_exists(self, email):
        try:
            email = Clients.objects.filter(email=email)
            if len(email) > 0:
                return True
            else:
                return False

        except ObjectDoesNotExist:
            return None

        except IndexError:
            return None

    def update_forum_username(self, client_id, forum_username):
        try:
            client = Clients.objects.get(pk=client_id)
            client.forum_username = forum_username
            client.save()
            return True

        except IndexError:
            return None

        except Exception:
            return None


class ClientsPp(models.Model):
    client_id = models.IntegerField()
    receiver_id = models.CharField(max_length=256, blank=True, null=True)
    residence_country = models.CharField(max_length=64, blank=True, null=True)
    first_name = models.CharField(max_length=256, blank=True, null=True)
    protection_eligibility = models.CharField(max_length=256, blank=True, null=True)
    txn_id = models.CharField(max_length=256, blank=True, null=True)
    charset = models.CharField(max_length=32, blank=True, null=True)
    payment_status = models.CharField(max_length=256, blank=True, null=True)
    verify_sign = models.CharField(max_length=256, blank=True, null=True)
    business = models.CharField(max_length=256, blank=True, null=True)
    item_name = models.CharField(max_length=128, blank=True, null=True)
    mc_gross = models.CharField(max_length=16, blank=True, null=True)
    receiver_email = models.CharField(max_length=256, blank=True, null=True)
    subscr_id = models.CharField(max_length=256, blank=True, null=True)
    last_name = models.CharField(max_length=256, blank=True, null=True)
    payer_email = models.CharField(max_length=256, blank=True, null=True)
    contact_phone = models.CharField(max_length=64, blank=True, null=True)
    txn_type = models.CharField(max_length=256, blank=True, null=True)
    payment_type = models.CharField(max_length=64, blank=True, null=True)
    payment_gross = models.CharField(max_length=16, blank=True, null=True)
    ipn_track_id = models.CharField(max_length=256, blank=True, null=True)
    payer_id = models.CharField(max_length=256, blank=True, null=True)
    mc_fee = models.CharField(max_length=16, blank=True, null=True)
    payer_status = models.CharField(max_length=64, blank=True, null=True)
    custom = models.CharField(max_length=16, blank=True, null=True)
    mc_currency = models.CharField(max_length=64, blank=True, null=True)
    payment_date = models.CharField(max_length=64, blank=True, null=True)
    payment_fee = models.CharField(max_length=16, blank=True, null=True)
    transaction_subject = models.CharField(max_length=256, blank=True, null=True)
    notify_version = models.CharField(max_length=32, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'clients_pp'

    def get_user_payments(self, client_id):
        try:
            payments = ClientsPp.objects.filter(id=client_id).last()
            return payments

        except ObjectDoesNotExist:
            return False

        except IndexError:
            return False

    def transaction_exists(self, client_id, payment_date):
        try:
            transaction = ClientsPp.objects.filter(client_id=client_id).filter(payment_date=payment_date)
            if transaction.count() > 0:
                return True
            else:
                return False

        except ObjectDoesNotExist:
            return False

        except IndexError:
            return None

    def save_client_pp(self, client_id, receiver_id, residence_country, first_name,
                       protection_eligibility,
                       txn_id,
                       charset,
                       payment_status,
                       verify_sign,
                       business,
                       item_name,
                       mc_gross,
                       receiver_email,
                       subscr_id,
                       last_name,
                       payer_email,
                       contact_phone,
                       txn_type,
                       payment_type,
                       payment_gross,
                       ipn_track_id,
                       payer_id,
                       mc_fee,
                       payer_status,
                       custom,
                       mc_currency,
                       payment_date,
                       payment_fee,
                       transaction_subject,
                       notify_version):

        try:
            client_pp = ClientsPp(client_id=client_id, receiver_id=receiver_id, residence_country=residence_country,
                                  first_name=first_name,
                                  protection_eligibility=protection_eligibility,
                                  txn_id=txn_id,
                                  charset=charset,
                                  payment_status=payment_status,
                                  verify_sign=verify_sign,
                                  business=business,
                                  item_name=item_name,
                                  mc_gross=mc_gross,
                                  receiver_email=receiver_email,
                                  subscr_id=subscr_id,
                                  last_name=last_name,
                                  payer_email=payer_email,
                                  contact_phone=contact_phone,
                                  txn_type=txn_type,
                                  payment_type=payment_type,
                                  payment_gross=payment_gross,
                                  ipn_track_id=ipn_track_id,
                                  payer_id=payer_id,
                                  mc_fee=mc_fee,
                                  payer_status=payer_status,
                                  custom=custom,
                                  mc_currency=mc_currency,
                                  payment_date=payment_date,
                                  payment_fee=payment_fee,
                                  transaction_subject=transaction_subject,
                                  notify_version=notify_version)
            client_pp.save()
            return True

        except Exception:
            return False


class DownloadStats(models.Model):
    download_time = models.CharField(max_length=64)
    download_file = models.CharField(max_length=64)

    class Meta:
        managed = False
        db_table = 'download_stats'

    def save_download(self, download_time, download_file):
        try:
            download = DownloadStats(download_time=download_time, download_file=download_file)
            download.save(using='stats')
            return True

        except ObjectDoesNotExist:
            return None
        except IndexError:
            return None


class Invitations(models.Model):
    client_id = models.IntegerField()
    client_email = models.CharField(max_length=256)
    invite_email = models.CharField(max_length=256)
    invite_number = models.CharField(max_length=64, blank=True, null=True)
    invite_date = models.CharField(max_length=256)
    confirmed = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'invitations'

    def count_invitations(self, client_id):
        try:
            invitations = Invitations.objects.filter(client_id=client_id).count()
            return invitations

        except ObjectDoesNotExist:
            return False

        except IndexError:
            return False

    def get_invites(self, client_id):
        try:
            invitations = Invitations.objects.filter(client_id=client_id)
            return invitations

        except ObjectDoesNotExist:
            return False

        except IndexError:
            return False

    def save_invitation(self, client_id=client_id, client_email=client_email, invite_email=invite_email, invite_number=invite_number, invite_date=invite_date, confirmed=confirmed):
        try:
            invitation = Invitations(client_id=client_id, client_email=client_email, invite_email=invite_email, invite_number=invite_number, invite_date=invite_date, confirmed=confirmed)
            invitation.save()
            return True

        except ObjectDoesNotExist:
            return None
        except IndexError:
            return None


class Logs(models.Model):
    level = models.CharField(max_length=16)
    user_level = models.CharField(max_length=16, blank=True, null=True)
    ip_address = models.CharField(max_length=16, blank=True, null=True)
    user = models.CharField(max_length=128, blank=True, null=True)
    source = models.CharField(max_length=64)
    message = models.TextField()
    timestamp = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'logs'


class NotificationRead(models.Model):
    notification_id = models.IntegerField()
    client_id = models.IntegerField()
    status = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'notification_read'

    def get_ids(self, client_id):
        try:
            notifications = NotificationRead.objects.filter(client_id=client_id).values('id')
            return notifications
        except Exception:
            return False

    def check_read(self, client_id, notification_id):
        try:
            NotificationRead.objects.filter(client_id=client_id).filter(id=notification_id)
            return True

        except ObjectDoesNotExist:
            return False

        except IndexError:
            return False

    def mark_as_read(self, notification_id=notification_id, client_id=client_id, status=status):
        try:
            notificatio_read = NotificationRead(notification_id=notification_id, client_id=client_id, status=1)
            notificatio_read.save()
            return True

        except ObjectDoesNotExist:
            return False

        except IndexError:
            return False


class Notifications(models.Model):
    client_id = models.IntegerField(blank=True, null=True)
    title = models.CharField(max_length=512)
    message = models.TextField()
    date_time = models.CharField(max_length=64)
    status = models.CharField(max_length=32, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'notifications'

    def get_notifications(self, clientid):
        try:
            notifications = Notifications.objects.filter(Q(client_id=clientid) | Q(client_id=0)).order_by('-id')
            return notifications

        except ObjectDoesNotExist:
            return False

        except IndexError:
            return False

    def notification_details(self, notification_id):
        try:
            notification = Notifications.objects.get(id=notification_id)
            return notification

        except ObjectDoesNotExist:
            return None

        except IndexError:
            return False

    def get_ids(self, clientid):
        try:
            notifications = Notifications.objects.filter(Q(client_id=clientid) | Q(client_id=0)).values('id')
            return notifications
        except Exception:
            return False

    def get_title(self, notification_id):
        try:
            title = Notifications.objects.filter(pk=notification_id).values('title')
            return title
        except Exception:
            return False


class Storage(models.Model):
    project_id = models.CharField(max_length=45)
    project = models.CharField(max_length=45, blank=True, null=True)
    domain = models.CharField(max_length=45)
    status = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'storage'


class Openstack(models.Model):
    domain = models.CharField(max_length=45)
    username = models.CharField(max_length=64, blank=True, null=True)
    user_id = models.CharField(max_length=128, null=True)
    password = models.CharField(max_length=45)
    project_id = models.CharField(max_length=128, null=True)
    group = models.CharField(max_length=45, null=True)
    group_id = models.CharField(max_length=128, null=True)
    role = models.CharField(max_length=45, null=True)
    role_id = models.CharField(max_length=128, null=True)
    identity_api = models.CharField(max_length=45)
    identity_api_v2 = models.CharField(max_length=45, blank=True, null=True)
    network_api = models.CharField(max_length=45)
    telemetry_api = models.CharField(max_length=45, blank=True, null=True)
    storage_api = models.CharField(max_length=45, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'openstack'

    def details(self):
        try:
            details = Openstack.objects.get()
            return details

        except ObjectDoesNotExist:
            return False

        except IndexError:
            return False


class PromoCodes(models.Model):
    name = models.CharField(max_length=512, blank=True, null=True)
    email = models.CharField(max_length=256, blank=True, null=True)
    code = models.CharField(max_length=64)
    created = models.CharField(max_length=64)
    expires = models.CharField(max_length=64)
    expired = models.IntegerField(blank=True, null=True)
    promo = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'promo_codes'

    def promo_exists(self, promo_code):
        try:
            code = PromoCodes.objects.filter(code=promo_code)
            if len(code) > 0:
                return True
            else:
                return False

        except ObjectDoesNotExist:
            return None
        except IndexError:
            return None

    def set_expired(self, promo_code):
        try:
            promo = PromoCodes.objects.get(code=promo_code)
            promo.expired = 1
            promo.save()
            return True

        except IndexError:
            return None

        except ObjectDoesNotExist:
            return None


class PromoUploads(models.Model):
    file_name = models.CharField(max_length=512)
    random_name = models.CharField(max_length=32)
    code = models.CharField(max_length=64)

    class Meta:
        managed = False
        db_table = 'promo_uploads'

    def get_files(self, promo_code):
        try:
            files = PromoUploads.objects.filter(code=promo_code)
            return files

        except ObjectDoesNotExist:
            return False

        except IndexError:
            return False


class Plans(models.Model):
    plan = models.CharField(max_length=64, blank=True, null=True)
    price_month = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    price_year = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    size = models.IntegerField(blank=True, null=True)
    description = models.TextField()

    class Meta:
        managed = False
        db_table = 'plans'

    def get_plan_details(self, plan):
        try:
            plan = Plans.objects.get(plan=plan)
            return plan

        except ObjectDoesNotExist:
            return None

        except IndexError:
            return False


class Support(models.Model):
    client_id = models.IntegerField()
    instance_id = models.CharField(max_length=256)
    subject = models.CharField(max_length=512, blank=True, null=True)
    priority = models.CharField(max_length=8)
    description = models.TextField()
    date = models.IntegerField()
    closed = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'support'


class SupportAttachment(models.Model):
    client_id = models.IntegerField()
    support_id = models.IntegerField()
    attachment_url = models.CharField(max_length=512)
    date = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'support_attachment'


class SupportMessages(models.Model):
    support_id = models.IntegerField()
    from_user = models.CharField(max_length=512)
    client_id = models.IntegerField()
    admin_id = models.IntegerField(blank=True, null=True)
    admin_name = models.CharField(max_length=128, blank=True, null=True)
    message = models.TextField()
    date = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'support_messages'


class TempData(models.Model):
    client_id = models.IntegerField()
    password = models.CharField(max_length=512)
    plan = models.CharField(max_length=45)
    bucket = models.CharField(max_length=64)
    forum_username = models.CharField(max_length=512)

    class Meta:
        managed = False
        db_table = 'temp_data'
