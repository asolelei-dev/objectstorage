from django.http import HttpResponse
from django.template.loader import render_to_string
from utils.session.customers import loginRequired
from ..models import models
from dashboard.forms import profileForm
import json
from django.http import Http404
import time
from utils.openstack.identity import Identity as identitylib
from utils.mailer.mailer import Mailer as mailer
from django.utils.crypto import get_random_string
from utils.openstack.swift_client import SwiftClient as swift_client


@loginRequired
def getStats(request):
    if request.is_ajax():
        if request.method == 'GET':
            try:
                client_id = request.session['user_id']
                clients = models.Clients()
                client_details = clients.get_client_details(client_id)
                billing_object = models.Billing()
                client_billing = billing_object.get_client_billing(client_id)
                plans = models.Plans()
                plan = plans.get_plan_details(client_billing.plan)
                swift = swift_client()

                container_details = swift.container_details(client_details.email)
                usedStorage = container_details['x-container-bytes-used']
                objectNumber = container_details['x-container-object-count']

                html = render_to_string('dashboard/partials/stats.html', {'plan': plan.plan, 'capacity': plan.size, 'used': usedStorage, 'files': objectNumber})
                data = {"response": 1, "template": html}
                return HttpResponse(json.dumps(data), content_type='application/json')
            except Exception:
                data = {"response": 0, "message": "Unable to process the request."}
                return HttpResponse(json.dumps(data), content_type='application/json')

        else:
            raise Http404

    else:
        raise Http404


@loginRequired
def s3credentials(request):
    if request.is_ajax():
        if request.method == 'GET':
            try:
                client_id = request.session['user_id']
                storage = models.ClientStorage()
                client_storage = storage.getStorageDetails(client_id)
                html = render_to_string('dashboard/partials/s3credentials.html', {'key': client_storage.ec2_access_key, 'secretKey': client_storage.ec2_secret_key})
                data = {"response": 1, "template": html}
                return HttpResponse(json.dumps(data), content_type='application/json')
            except Exception:
                data = {"response": 0, "message": "Unable to process the request."}
                return HttpResponse(json.dumps(data), content_type='application/json')

        else:
            raise Http404

    else:
        raise Http404


def verify(request):
    return None


@loginRequired
def changePassword(request):
    if request.is_ajax():
        if request.method == 'POST':
            form = profileForm.changePassword(request.POST)
            if form.is_valid():
                client_id = request.session['user_id']
                current_password = form.cleaned_data.get('current_password')
                new_password = form.cleaned_data.get('new_password')
                repeat_password = form.cleaned_data.get('repeat_password')
                clients = models.Clients()
                check = clients.checkPassword(client_id, current_password)
                if not check:
                    data = {"response": 0, "message": "Incorrect current password entered."}
                    return HttpResponse(json.dumps(data), content_type='application/json')
                if new_password != repeat_password:
                    data = {"response": 0, "message": "New passwords do not match."}
                    return HttpResponse(json.dumps(data), content_type='application/json')

                if check and new_password == repeat_password:
                    identity = identitylib()
                    token = identity.tokenv3()
                    client_details = clients.get_client_details(client_id)
                    change_password = identity.changeUserPassword(token, client_details.email, client_details.os_user_id, new_password)
                    if change_password is True:
                        savePassword = clients.updatePassword(client_id, new_password)
                        if savePassword:
                            data = {"response": 1, "message": "Your password has been changed."}
                            return HttpResponse(json.dumps(data), content_type='application/json')
                        else:
                            data = {"response": 0, "message": "Something went wrong. Please try again or contact support."}
                            return HttpResponse(json.dumps(data), content_type='application/json')
                    else:
                        data = {"response": 0, "message": "Something went wrong. Please try again or contact support."}
                        return HttpResponse(json.dumps(data), content_type='application/json')
            else:
                data = {"response": 0, "message": "Something went wrong. Please try again or contact support."}
                return HttpResponse(json.dumps(data), content_type='application/json')
        else:
            raise Http404

    else:
        raise Http404


@loginRequired
def addInfo(request):
    if request.is_ajax():
        if request.method == 'POST':
            form = profileForm.updateInfo(request.POST)
            if form.is_valid():
                client_id = request.session['user_id']
                company = form.cleaned_data.get('company')
                address = form.cleaned_data.get('address')
                phone_number = form.cleaned_data.get('phone_number')
                clients = models.Clients()
                change_info = clients.updateInfo(client_id=client_id, company=company, address=address, phone=phone_number)
                if change_info:
                    data = {"response": 1, "message": "Your information has been added to your profile."}
                    return HttpResponse(json.dumps(data), content_type='application/json')
                else:
                    data = {"response": 0, "message": "Something went wrong. Please try again or contact support."}
                    return HttpResponse(json.dumps(data), content_type='application/json')

            else:
                data = {"response": 0, "message": "Something went wrong. Please try again or contact support."}
                return HttpResponse(json.dumps(data), content_type='application/json')
        else:
            raise Http404
    else:
        raise Http404


@loginRequired
def sendInvites(request):
    if request.is_ajax():
        if request.method == 'POST':
            form = profileForm.SendInvites(request.POST)
            if form.is_valid():
                client_id = request.session['user_id']
                emails = form.cleaned_data.get('emails')
                clients = models.Clients()
                invitations = models.Invitations()
                client_details = clients.get_client_details(client_id)
                invite_emails = emails.split(', ')
                mail = mailer()
                existing_users = []
                for e in invite_emails:
                    exists = clients.email_exists(e)
                    if exists:
                        existing_users.append(e)
                    else:
                        # Save to invitation
                        invite_date = time.strftime("%B %d, %Y at %X")
                        invite_number = get_random_string(length=16)
                        invitations.save_invitation(client_id=client_id, client_email=client_details.email, invite_email=e, invite_number=invite_number, invite_date=invite_date, confirmed=False)
                        # Send emails
                        mail.invite_email(client_details.email, e, invite_number)

                if not existing_users:
                    data = {"response": 1, "message": "Invitations send successfully."}
                    return HttpResponse(json.dumps(data), content_type='application/json')

            else:
                data = {"response": 0, "message": "Something went wrong. Please try again or contact support."}
                return HttpResponse(json.dumps(data), content_type='application/json')
        else:
            raise Http404
    else:
        raise Http404
