from django.shortcuts import render
from utils.session.customers import loginRequired
from dashboard.tables import usersTable
from ..models import models
from django.contrib import messages
from django.shortcuts import redirect
import json
from django.http import HttpResponse
from django.http import Http404
import dateutil.parser
from django_tables2 import RequestConfig
from utils.openstack.swift_client import SwiftClient as swift_client
from utils.helpers import config
import os
import urllib.parse
import math


def format_size(size):
    if size == 0:
        return '0 B'
    size_name = ('B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB')
    i = int(math.floor(math.log(size, 1024)))
    p = math.pow(1024, i)
    s = round(size / p, 3)
    return '{} {}'.format(s, size_name[i])


@loginRequired
def index(request, folder=None):
    client_id = request.session['user_id']
    clients = models.Clients()
    storage = models.ClientStorage()
    segments = request.path_info.strip('/').split('/')
    del segments[0:2]
    # Setup Paginator
    page = request.GET.get('page')

    folder = '/'.join(segments) + "/"
    client_details = clients.get_client_details(client_id)
    storage_details = storage.getStorageDetails(client_id)
    swift = swift_client()
    objects = swift.get_objects_head(storage_details.container)
    object_num = objects['x-container-object-count']
    num_pages = math.ceil(int(object_num) / 100)
    current_page = 0
    if page is not None:
        current_page = int(page)

    pages = []
    marker = ''
    if not page or page == '1':
        if folder == '/':
            objects = swift.get_objects(storage_details.container, limit=100)
            all_objects = swift.get_objects(storage_details.container)
            num_objects = int(len(all_objects))
            num_pages = math.ceil(int(num_objects) / 100)
            if num_pages > 1:
                for i in range(1, int(num_pages) + 1):
                    pages.append(i)
        else:
            objects = swift.get_objects(storage_details.container, prefix=folder, limit=100)
            all_objects = swift.get_objects(storage_details.container, prefix=folder)
            num_objects = int(len(all_objects))
            num_pages = math.ceil(int(num_objects) / 100)
            if num_pages > 1:
                for i in range(1, int(num_pages) + 1):
                    pages.append(i)
    else:
        if folder == '/':
            # Create pagination
            temp_objects = swift.get_objects(storage_details.container, limit=100)
            all_objects = swift.get_objects(storage_details.container)
            num_objects = int(len(all_objects))
            num_pages = math.ceil(int(num_objects) / 100)
            if num_pages > 1:
                for i in range(1, int(num_pages) + 1):
                    pages.append(i)
            # Get number of objects on the last page
            if int(page) == num_pages or int(page) > num_pages:
                start_position = ((int(page) - 1) * 100) - 1
                marker = temp_objects[start_position]['name']
                end_position = int(object_num)
                objects = swift.get_objects(storage_details.container, marker=marker, limit=(end_position - start_position))
            else:
                end_position = (int(page) * 100)
                end_marker = temp_objects[end_position]['name']
                objects = swift.get_objects(storage_details.container, marker=marker, end_marker=end_marker, limit=100)
        else:
            temp_objects = swift.get_objects(storage_details.container, prefix=folder, limit=100)
            all_objects = swift.get_objects(storage_details.container, prefix=folder)
            num_objects = int(len(all_objects))
            num_pages = math.ceil(int(num_objects) / 100)
            if num_pages > 1:
                for i in range(1, int(num_pages) + 1):
                    pages.append(i)
            # Get number of objects on the last page
            if int(page) == num_pages or int(page) > num_pages:
                start_position = ((int(page) - 1) * 100) - 1
                marker = temp_objects[start_position]['name']
                end_position = int(object_num)
                objects = swift.get_objects(storage_details.container, prefix=folder, marker=marker, limit=(end_position - start_position))
            else:
                start_position = ((int(page) - 1) * 100) - 1
                marker = temp_objects[start_position]['name']
                end_position = (int(page) * 100)
                end_marker = temp_objects[end_position]['name']
                objects = swift.get_objects(storage_details.container, prefix=folder, marker=marker, end_marker=end_marker, limit=100)

    count = -1
    link_list = []
    for i in segments:
        count += 1
        if len(segments) > 1:
            if count == 0:
                link_list.append(config.SEGMENT_LINK.format(i, i))
            else:
                link_list.append(config.SEGMENT_LINK_NEXT.format(segments[count - 1], segments[count], i))
        else:
            link_list.append(config.SEGMENT_LINK.format(i, i))

    table_settings = []
    for value in objects:
        if getattr(value, 'content_type') == 'application/pseudo-folder':
            if '/' in getattr(value, 'name'):
                parts = getattr(value, 'name').split('/')
                data = {'name': config.OBJECT_NAME.format(getattr(value, 'name'), parts[-1]),
                        'size': '',
                        'created': '',
                        'options': ''
                        }
            else:
                data = {'name': config.OBJECT_NAME.format(getattr(value, 'name'), getattr(value, 'name')),
                        'size': '',
                        'created': '',
                        'options': ''

                        }
            table_settings.append(data)

    for value in objects:
        if getattr(value, 'content_type') != 'application/pseudo-folder':
            if '/' in getattr(value, 'name'):
                if not getattr(value, 'name').endswith('/'):
                    parts = getattr(value, 'name').split('/')
                    size = format_size(getattr(value, 'bytes'))
                    data = {'name': parts[-1],
                            'size': '{}'.format(str(size)),
                            'created': dateutil.parser.parse(getattr(value, 'last_modified')).strftime("%Y-%m-%d %H:%M"),
                            'options': config.OBJECT_OPTIONS.format(urllib.parse.quote(getattr(value, 'name')), urllib.parse.quote(getattr(value, 'name')))}
                else:
                    continue

            else:
                size = format_size(getattr(value, 'bytes'))
                data = {'name': getattr(value, 'name'),
                        'size': '{}'.format(str(size)),
                        'created': dateutil.parser.parse(getattr(value, 'last_modified')).strftime("%Y-%m-%d %H:%M"),
                        'options': config.OBJECT_OPTIONS.format(urllib.parse.quote(getattr(value, 'name')), urllib.parse.quote(getattr(value, 'name')))}

            table_settings.append(data)

    table = usersTable.ObjectsTable(table_settings)
    RequestConfig(request, paginate={"per_page": 100}).configure(table)
    return render(request, 'dashboard/objects.html', {'table': table, 'client_email': client_details.email, 'folder': link_list,
                                                      'client_id': client_id,
                                                      'segments': folder,
                                                      'page_num': pages,
                                                      'current_page': current_page})


@loginRequired
def download_object(request):
    if request.method == 'GET':
        client_id = request.session['user_id']
        clients = models.Clients()
        client_details = clients.get_client_details(client_id)
        object_path = urllib.parse.unquote(request.GET['path'])
        swift = swift_client()
        objects = swift.get_object(client_details.email, object_path)

        # Add the original file extension back on if it wasn't preserved in the name given to the object.
        filename = object_path.rsplit('/')[-1]
        if not os.path.splitext(objects.name)[1] and objects.orig_name:
            name, ext = os.path.splitext(objects.orig_name)
            filename = "%s%s" % (filename, ext)
            # filename = '{}{}'.format(filename, ext)
        safe_name = filename.replace(",", "")
        response = HttpResponse(objects.data, content_type=objects.content_type)
        response['Content-Disposition'] = 'attachment; filename="{}"'.format(safe_name)
        return response

    else:
        raise Http404


@loginRequired
def upload_object(request):
    client_id = request.session['user_id']
    folder = request.POST.get('pseudo-folder')
    clients = models.Clients()
    client_details = clients.get_client_details(client_id)
    swift = swift_client()
    myfile = request.FILES['myfile']
    if folder == '/':
        swift.upload_object(client_details.email, myfile.name, myfile)
        messages.add_message(request, messages.SUCCESS, 'File {} uploaded successfully.'.format(myfile.name))
        return redirect('/dashboard/files/')
    else:
        swift.upload_object(client_details.email, folder + myfile.name, myfile)
        messages.add_message(request, messages.SUCCESS, 'File {} uploaded successfully.'.format(myfile.name))
        return redirect('/dashboard/files/{}'.format(folder))


@loginRequired
def delete_object(request):
    if request.is_ajax():
        if request.method == 'POST':
            client_id = request.session['user_id']
            clients = models.Clients()
            client_details = clients.get_client_details(client_id)
            object_path = urllib.parse.unquote(request.POST.get('path'))
            swift = swift_client()
            if '/' not in object_path:
                folder = ''
                swift.delete_object(client_details.email, object_path)
                try:
                    swift.delete_object(client_details.email, object_path)
                    data = {"response": 1, "path": folder}
                    return HttpResponse(json.dumps(data), content_type='application/json')

                except Exception:
                    swift.delete_object(client_details.email, object_path)
                    data = {"response": 0, "message": "An error occured while processing the request."}
                    return HttpResponse(json.dumps(data), content_type='application/json')
            else:
                split = object_path.split('/')
                del split[-1]
                folder = '/'.join(split)
                swift.delete_object(client_details.email, object_path)
                try:
                    swift.delete_object(client_details.email, object_path)
                    data = {"response": 1, "path": folder}
                    return HttpResponse(json.dumps(data), content_type='application/json')

                except Exception:
                    swift.delete_object(client_details.email, object_path)
                    data = {"response": 0, "message": "An error occured while processing the request."}
                    return HttpResponse(json.dumps(data), content_type='application/json')
        else:
            raise Http404
    else:
        raise Http404


@loginRequired
def create_folder(request):
    if request.method == 'POST':
        client_id = request.session['user_id']
        folder_name = request.POST['folder_name']
        folder = request.POST['pseudo-folder']
        if folder == '/':
            folder = ''

        clients = models.Clients()
        client_details = clients.get_client_details(client_id)
        try:
            swift = swift_client()
            swift.create_pseudo_folder(client_details.email, '{}{}/'.format(folder, folder_name))
            return redirect('/dashboard/files/{}{}/'.format(folder, folder_name))

        except Exception:
            messages.add_message(request, messages.SUCCESS, 'An error occured. Check the logs for more information')
            return redirect('/dashboard/files/{}{}/'.format(folder, folder_name))
