from django.shortcuts import render
from utils.session.customers import loginRequired
from ..models import models
from django.contrib import messages
from utils.helpers import config


@loginRequired
def index(request):
    client_id = request.session['user_id']
    clients = models.Clients()
    client_details = clients.get_client_details(client_id)
    billing_object = models.Billing()
    plans = models.Plans()
    client_billing = billing_object.get_client_billing(client_id)
    plan = plans.get_plan_details(client_billing.plan)

    if not client_details.first_payment and client_billing.plan != 'basic':
        messages.add_message(request, messages.INFO, config.BILLING_SUBSCRIBE)
        return render(request, 'dashboard/billing.html', {'customer_id': client_id, 'billing': client_billing, 'plan': plan, 'payment': client_details.first_payment})
    else:
        return render(request, 'dashboard/billing.html', {'customer_id': client_id, 'billing': client_billing, 'plan': plan, 'payment': client_details.first_payment})
