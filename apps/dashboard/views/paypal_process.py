from django.http import HttpResponse
from django.http import HttpResponseBadRequest
from ..models import models
from utils.openstack.swift_client import SwiftClient as swift_client
from utils.mailer.mailer import Mailer as mailer
import urllib.request
import urllib.parse
import requests
from django.views.decorators.csrf import csrf_exempt
import datetime
from dateutil import relativedelta


@csrf_exempt
def processs_ipn(request):
    if request.POST:
        paypal_url = 'https://www.paypal.com/cgi-bin/webscr?'
        # paypal_url = 'https://www.sandbox.paypal.com/cgi-bin/webscr?'
        query = 'cmd=_notify-validate'
        for key, value in request.POST.items():
            query += '&%s=%s' % (key, urllib.parse.quote(value))
        result = requests.post(paypal_url, data=query, timeout=10)

        # verification is passed
        if result.text == 'VERIFIED':
            if 'mc_gross' in request.POST:
                # Check if client_id exists in our database
                ids = str(request.POST['custom'])
                client_id, planid = ids.split('-')
                plan_id = int(planid)
                clients = models.Clients()
                client_exists = clients.client_exists(int(client_id))
                client_details = clients.get_client_details(int(client_id))
                storage = models.ClientStorage()
                clientspp = models.ClientsPp()
                if client_exists:
                    # save data to database
                    receiver_id = request.POST['receiver_id']
                    residence_country = request.POST['residence_country']
                    first_name = request.POST['first_name']
                    protection_eligibility = request.POST['protection_eligibility']
                    txn_id = request.POST['txn_id']
                    charset = request.POST['charset']
                    payment_status = request.POST['payment_status']
                    verify_sign = request.POST['verify_sign']
                    business = request.POST['business']
                    item_name = request.POST['item_name']
                    mc_gross = request.POST['mc_gross']
                    receiver_email = request.POST['receiver_email']
                    subscr_id = request.POST['subscr_id']
                    last_name = request.POST['last_name']
                    payer_email = request.POST['payer_email']
                    contact_phone = ''
                    txn_type = request.POST['txn_type']
                    payment_type = request.POST['payment_type']
                    payment_gross = request.POST['payment_gross']
                    ipn_track_id = request.POST['ipn_track_id']
                    payer_id = request.POST['payer_id']
                    mc_fee = request.POST['mc_fee']
                    payer_status = request.POST['payer_status']
                    custom = request.POST['custom']
                    mc_currency = request.POST['mc_currency']
                    payment_date = request.POST['payment_date']
                    payment_fee = request.POST['payment_fee']
                    transaction_subject = request.POST['transaction_subject']
                    notify_version = request.POST['notify_version']

                    check_transation = clientspp.transaction_exists(int(client_id), payment_date)

                    if not check_transation:
                        clientspp.save_client_pp(client_id=int(client_id), receiver_id=receiver_id, residence_country=residence_country,
                                                 first_name=first_name,
                                                 protection_eligibility=protection_eligibility,
                                                 txn_id=txn_id,
                                                 charset=charset,
                                                 payment_status=payment_status,
                                                 verify_sign=verify_sign,
                                                 business=business,
                                                 item_name=item_name,
                                                 mc_gross=mc_gross,
                                                 receiver_email=receiver_email,
                                                 subscr_id=subscr_id,
                                                 last_name=last_name,
                                                 payer_email=payer_email,
                                                 contact_phone=contact_phone,
                                                 txn_type=txn_type,
                                                 payment_type=payment_type,
                                                 payment_gross=payment_gross,
                                                 ipn_track_id=ipn_track_id,
                                                 payer_id=payer_id,
                                                 mc_fee=mc_fee,
                                                 payer_status=payer_status,
                                                 custom=custom,
                                                 mc_currency=mc_currency,
                                                 payment_date=payment_date,
                                                 payment_fee=payment_fee,
                                                 transaction_subject=transaction_subject,
                                                 notify_version=notify_version)
                    # Change customer plan
                    swift = swift_client()
                    billing = models.Billing()
                    openstack = models.Openstack()
                    openstack_details = openstack.details()
                    mail = mailer()
                    client_billing = billing.get_client_billing(int(client_id))
                    nextmonth = datetime.date.today() + relativedelta.relativedelta(months=1)
                    date = nextmonth.strftime("%B %d, %Y")
                    # containerSize
                    if plan_id == 1:
                        plan = "250 GB"
                        plan_size = str(1024 * 1024 * 1024 * 250)
                        if client_billing.plan == plan:
                            pass
                        else:
                            try:
                                # Update container size, update billing and update client storage
                                swift.update_container(client_details.email, {"x-container-meta-quota-bytes": plan_size, 'x-container-read': openstack_details.project_id + ':' + client_details.os_user_id + ',.rlistings', 'x-container-write': openstack_details.project_id + ':' + client_details.os_user_id, "is_public": False})
                                # Update billing
                                billing.update_billing(int(client_id), plan)
                                # Update client storage
                                storage.update_client_storage(int(client_id), plan_size)
                                # Send email
                                mail.payment(client_details.email, plan, date)

                            except Exception:
                                return HttpResponse(500)

                    elif plan_id == 2:
                        plan = "2 TB"
                        plan_size = str(1024 * 1024 * 1024 * 2000)
                        if client_billing.plan == plan:
                            pass
                        else:
                            try:
                                # Update container size
                                swift.update_container(client_details.email, {"x-container-meta-quota-bytes": plan_size, 'x-container-read': openstack_details.project_id + ':' + client_details.os_user_id + ',.rlistings', 'x-container-write': openstack_details.project_id + ':' + client_details.os_user_id, "is_public": False})
                                # Update billing
                                billing.update_billing(int(client_id), plan)
                                # Update client storage
                                storage.update_client_storage(int(client_id), plan_size)
                                # Send email
                                mail.payment(client_details.email, plan, date)

                            except Exception:
                                # Log error
                                return HttpResponse(500)

                    elif plan_id == 3:
                        plan = "10 TB"
                        plan_size = str(1024 * 1024 * 1024 * 10000)
                        if client_billing.plan == plan:
                            pass
                        else:
                            try:
                                # Update container size
                                swift.update_container(client_details.email, {"x-container-meta-quota-bytes": plan_size, 'x-container-read': openstack_details.project_id + ':' + client_details.os_user_id + ',.rlistings', 'x-container-write': openstack_details.project_id + ':' + client_details.os_user_id, "is_public": False})
                                # Update billing
                                billing.update_billing(int(client_id), plan)
                                # Update client storage
                                storage.update_client_storage(int(client_id), plan_size)
                                # Send email
                                mail.payment(client_details.email, plan, date)

                            except Exception:
                                # Log error
                                return HttpResponse(500)

                    return HttpResponse(status=200)
                else:
                    # client with this id is not registered with blomp - log event
                    return HttpResponseBadRequest()

            else:
                # Log error
                return HttpResponse(status=200)

        elif result.text == 'REVERSED' or result.text == 'REFUNDED':
            # Log event
            return HttpResponse(status=200)

        elif result.text == 'INVALID':
            # Log event
            return HttpResponse(status=200)

        else:
            return HttpResponse(status=200)

    else:
        return HttpResponse(status=200)
