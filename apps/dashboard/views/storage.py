
from django.shortcuts import render
from utils.session.customers import loginRequired
from dashboard.models import models
import datetime
import dateutil.parser
from utils.openstack.swift_client import SwiftClient as swift_client
import requests
from django.http import HttpResponse


def bytesConvert(size):
    # 2**10 = 1024
    power = 2**10
    n = 0
    unit = {0: 'B', 1: 'kB', 2: 'MB', 3: 'GB', 4: 'TB'}
    while size > power:
        size /= power
        n += 1
    return size, unit[n]


@loginRequired
def index(request):
    client_id = request.session['user_id']
    clients = models.Clients()
    client_payments = models.ClientsPp()
    billing_object = models.Billing()
    plans = models.Plans()
    storage = models.ClientStorage()
    client_details = clients.get_client_details(client_id)
    client_billing = billing_object.get_client_billing(client_id)
    plan = plans.get_plan_details(client_billing.plan)
    clientStorage = storage.getStorageDetails(client_id)

    swift = swift_client()
    containerDetails = swift.container_details(client_details.email)
    containerQuota = containerDetails['x-container-meta-quota-bytes']
    usedStorage = containerDetails['x-container-bytes-used']
    objectNumber = containerDetails['x-container-object-count']
    registered = client_details.regdate.split('at')[0]
    if plan.plan != 'basic':
        payment_date = client_payments.get_user_payments(client_id)
        if payment_date:
            client_date = payment_date.payment_date.split(' ')
            cd = '{} {} {}'.format(client_date[1], client_date[2], client_date[3])
            cd_prepare = dateutil.parser.parse(cd)
            paidon = cd_prepare.strftime('%B %d, %Y')
            endDate = cd_prepare + datetime.timedelta(days=30)
            expireDate = endDate.strftime('%B %d, %Y')
            return render(request, 'dashboard/dashboard.html', {'customer_id': client_id,
                                                                'billing': client_billing,
                                                                'plan': bytesConvert(int(containerQuota)),
                                                                'plan_name': plan.plan,
                                                                'details': client_details,
                                                                'bucket': clientStorage.container,
                                                                'usedStorage': int(usedStorage),
                                                                'totalFiles': objectNumber,
                                                                'paidon': paidon,
                                                                'expireDate': expireDate,
                                                                'registered': registered})
        else:
            paidon = ''
            expireDate = 'Unlimited'
            return render(request, 'dashboard/dashboard.html', {'customer_id': client_id,
                                                                'billing': client_billing,
                                                                'plan': bytesConvert(int(containerQuota)),
                                                                'plan_name': plan.plan,
                                                                'details': client_details,
                                                                'bucket': clientStorage.container,
                                                                'usedStorage': int(usedStorage),
                                                                'totalFiles': objectNumber,
                                                                'paidon': paidon,
                                                                'expireDate': expireDate,
                                                                'registered': registered})

    return render(request, 'dashboard/dashboard.html', {'customer_id': client_id,
                                                        'billing': client_billing,
                                                        'plan': bytesConvert(int(containerQuota)),
                                                        'plan_name': plan.plan,
                                                        'details': client_details,
                                                        'bucket': clientStorage.container,
                                                        'usedStorage': int(usedStorage),
                                                        'totalFiles': objectNumber,
                                                        'registered': registered})


@loginRequired
def advanced(request):
    client_id = request.session['user_id']
    clients = models.Clients()
    storage = models.ClientStorage()
    client_details = clients.get_client_details(client_id)
    client_storage = storage.getStorageDetails(client_id)
    return render(request, 'dashboard/advanced.html', {'customer_id': client_id, 'details': client_details,
                                                       'bucket': client_storage.container})


@loginRequired
def download(request, file):
    download = models.DownloadStats()
    if file == 'blompgo-win10':
        url = 'https://download.blomp.com/Blomp/blompgo/blompgo_win10_x64.exe'
        filename = 'blompgo_win10_x64.exe'
        download_time = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        r = requests.get(url)
        response = HttpResponse(r.content)
        response['Content-Disposition'] = 'attachment; filename={}'.format(filename)
        download.save_download(download_time, 'BlompGo Win 10')
        return response

    elif file == 'blompgo-win7':
        url = 'https://download.blomp.com/Blomp/blompgo/blompgo_win7_x64.exe'
        filename = 'blompgo_win7_x64.exe'
        download_time = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        r = requests.get(url)
        response = HttpResponse(r.content)
        response['Content-Disposition'] = 'attachment; filename={}'.format(filename)
        download.save_download(download_time, 'BlompGo Win 7')
        return response

    elif file == 'blomplive-win':
        url = 'https://download.blomp.com/BlompLive/windows/BlompInstall.0.5.5.exe'
        filename = 'BlompInstall.0.5.5.exe'
        download_time = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        r = requests.get(url)
        response = HttpResponse(r.content)
        response['Content-Disposition'] = 'attachment; filename={}'.format(filename)
        download.save_download(download_time, 'BlompLive Windows')
        return response

    elif file == 'blomplive-mac':
        url = 'https://download.blomp.com/BlompLive/mac/BlompInstall.0.5.5.app.zip'
        filename = 'BlompInstall.0.5.5.app.zip'
        download_time = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        r = requests.get(url)
        response = HttpResponse(r.content)
        response['Content-Disposition'] = 'attachment; filename={}'.format(filename)
        download.save_download(download_time, 'BlompLive Mac')
        return response

    elif file == 'blomplive-linux':
        url = 'https://download.blomp.com/BlompLive/linux/BlompInstall.0.5.5'
        filename = 'BlompInstall.0.5.5'
        download_time = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        r = requests.get(url)
        response = HttpResponse(r.content)
        response['Content-Disposition'] = 'attachment; filename={}'.format(filename)
        download.save_download(download_time, 'BlompLive Linux')
        return response
