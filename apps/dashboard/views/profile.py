from django.shortcuts import render
from utils.session.customers import loginRequired
from dashboard.models import models
from utils.openstack.swift_client import SwiftClient as swift_client
from dashboard.forms import profileForm
from utils.phpbb.phpbb import Phpbb as phpbb_lib
from frontend.tasks import move_files
from django.http import Http404
from django.http import HttpResponse
import json
from django.contrib import messages
from django.shortcuts import redirect


@loginRequired
def index(request):
    client_id = request.session['user_id']
    billing_object = models.Billing()
    clients = models.Clients()
    client_billing = billing_object.get_client_billing(client_id)
    client_details = clients.get_client_details(client_id)
    storage = models.ClientStorage()
    storage_details = storage.getStorageDetails(client_id)
    swift = swift_client()
    phpbb = phpbb_lib()
    containerDetails = swift.container_details(storage_details.container)
    containerQuota = containerDetails['x-container-meta-quota-bytes']
    usedStorage = containerDetails['x-container-bytes-used']
    availableStorage = int(containerQuota) - int(usedStorage)
    forum_username = client_details.forum_username
    if not forum_username:
        forum_username = phpbb.get_username(client_details.email)
        if not forum_username:
            forum_username = 'None Created'
        else:
            clients.update_forum_username(client_id, forum_username)

    return render(request, 'dashboard/profile.html', {'billing': client_billing, 'plan': containerQuota, 'used': int(usedStorage),
                                                      'available': availableStorage,
                                                      'client': client_details,
                                                      'forum_username': forum_username})


@loginRequired
def invite(request):
    client_id = request.session['user_id']
    form = profileForm.SendInvites()
    invitations = models.Invitations()
    invitation_number = invitations.count_invitations(client_id)
    if not invitation_number:
        invitation_number = 0
    invites = invitations.get_invites(client_id)
    return render(request, 'dashboard/invites.html', {'form': form, 'invite_number': invitation_number, 'emails': invites})


@loginRequired
def promo(request):
    form = profileForm.PromoCode()
    return render(request, 'dashboard/promo.html', {'form': form})


@loginRequired
def list_promo_files(request, promo_code):
    if request.is_ajax():
        if request.method == 'GET':
            promo_files = models.PromoUploads()
            if promo_code == '':
                data = {"response": 0, "message": "Please enter a valid promo code."}
                return HttpResponse(json.dumps(data), content_type='application/json')
            else:
                files = promo_files.get_files(promo_code)
                if not files:
                    data = {"response": 0, "message": "Promo code you enter does not exists."}
                    return HttpResponse(json.dumps(data), content_type='application/json')
                file_list = []
                for i in files:
                    file_list.append(i.file_name)
                data = {"response": 1, "files": file_list}
                return HttpResponse(json.dumps(data), content_type='application/json')
        else:
            raise Http404
    else:
        raise Http404


@loginRequired
def transfer_files(request):
    if request.POST:
        form = profileForm.PromoCode(request.POST)
        if form.is_valid():
            promo_code = form.cleaned_data.get('promo_code')
            if promo_code == '':
                messages.add_message(request, messages.ERROR, 'Please enter a valid promo code.')
                return redirect('/dashboard/profile/promo/')
            else:
                promo_files = models.PromoUploads()
                files = promo_files.get_files(promo_code)
                if not files:
                    messages.add_message(request, messages.ERROR, 'No files are associated with the promo code you entered.')
                    return redirect('/dashboard/profile/promo/')
                else:
                    clients = models.Clients()
                    client_id = request.session['user_id']
                    client_details = clients.get_client_details(client_id)
                    move_files.delay(promo, client_details.email)
                    messages.add_message(request, messages.SUCCESS, 'Your files are being transfered to your storage.')
                    return redirect('/dashboard/profile/promo/')

    else:
        raise Http404
