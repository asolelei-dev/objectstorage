from django.shortcuts import render
from utils.session.customers import loginRequired
from ..models import models
from django.core.paginator import Paginator


@loginRequired
def index(request):
    client_id = request.session['user_id']
    n = models.Notifications()
    all_notifications = n.get_notifications(client_id)
    paginator = Paginator(all_notifications, 20)
    page = request.GET.get('page')
    notifications = paginator.get_page(page)
    return render(request, 'dashboard/notifications.html', {'notify': notifications})


@loginRequired
def read(request, notification_id):
    client_id = request.session['user_id']
    n = models.Notifications()
    nr = models.NotificationRead()
    # Mark as read
    nr.mark_as_read(notification_id, client_id, 1)
    check_read = nr.check_read(client_id, notification_id)
    details = n.notification_details(notification_id)
    return render(request, 'dashboard/view_notification.html', {'details': details, 'check_read': check_read})
