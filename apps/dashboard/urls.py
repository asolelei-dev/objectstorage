from django.conf.urls import url
from dashboard.views import storage
from dashboard.views import billing
from dashboard.views import profile
from dashboard.views import data
from dashboard.views import notifications
from dashboard.views import explorer
from dashboard.views import paypal_process

urlpatterns = [
    url(r'^$', storage.index, name="dashboard"),
    url(r'^advanced', storage.advanced, name="advanced"),
    url(r'^billing', billing.index, name="billing"),
    url(r'^download/(.*)/', storage.download, name="download"),
    url(r'^files/(.*)$', explorer.index, name="my files"),
    url(r'^payments/process', paypal_process.processs_ipn, name="process_ipn"),
    url(r'^storage/upload_object/', explorer.upload_object, name="upload object"),
    url(r'^storage/download_object/', explorer.download_object, name="download object"),
    url(r'^storage/delete_object/', explorer.delete_object, name="delete object"),
    url(r'^storage/create_folder/', explorer.create_folder, name="create pseudofolder"),
    url(r'^profile/$', profile.index, name="profile"),
    url(r'^profile/promo/$', profile.promo, name="promo codes"),
    url(r'^profile/promo/list/(.*)', profile.list_promo_files, name="list promo files"),
    url(r'^profile/promo/transfer/', profile.transfer_files, name="transfer promo files"),
    url(r'^profile/invite/$', profile.invite, name="invite"),
    url(r'^profile/invite/send/', data.sendInvites, name="send invite"),
    url(r'^notifications/$', notifications.index, name="notifications"),
    url(r'^notifications/read/(.*)', notifications.read, name="notifications"),
    url(r'^data/stats/', data.getStats, name="stats"),
    url(r'^data/s3credentials/', data.s3credentials, name="S3 credentials"),
    url(r'^data/change_password/', data.changePassword, name="change password"),
    url(r'^data/add_info/', data.addInfo, name="add information"),
]
