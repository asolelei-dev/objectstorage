import django_tables2 as tables


class ObjectsTable(tables.Table):
    name = tables.TemplateColumn("{{ value|safe }}")
    size = tables.TemplateColumn("{{ value|safe }}")
    created = tables.TemplateColumn("{{ value|safe }}")
    options = tables.TemplateColumn("{{ value|safe }}")

    class Meta:
        attrs = {'class': 'table'}
