from django import template
from ..models import models
register = template.Library()


def diff(notfications, read):
    read_notifications = set(read)
    return [item for item in notfications if item not in read_notifications]


@register.simple_tag
def get_notifications(client_id):
    notifications = models.Notifications()
    read = models.NotificationRead()
    notification_ids = notifications.get_ids(client_id)
    nids = []
    for i in notification_ids:
        nids.append(i['id'])

    rids = []
    read_notifications_ids = read.get_ids(client_id)
    for j in read_notifications_ids:
        rids.append(j['id'])

    unread = diff(nids, rids)
    titles = []
    for k in unread:
        title = notifications.get_title(k)
        text = '<ul class="media-list dropdown-content-body"><li class="media"><div class="media-left"><span class="text-info"><i class="icon-list3"></i></span></div><div class="media-body"><a href="/dashboard/notifications/read/' + str(k) + '" class="media-heading"><span class="text-semibold">' + title[0]['title'] + '</span></a></div></li></ul>'
        titles.append(text)

    if not titles:
        return ''
    else:
        return titles


@register.simple_tag
def count_notifications(client_id):
    notifications = models.Notifications()
    read = models.NotificationRead()
    notification_ids = notifications.get_ids(client_id)
    nids = []
    for i in notification_ids:
        nids.append(i['id'])

    rids = []
    read_notifications_ids = read.get_ids(client_id)
    for j in read_notifications_ids:
        rids.append(j['id'])

    unread = diff(nids, rids)
    return len(unread)
