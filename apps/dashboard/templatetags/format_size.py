from django import template
import math
register = template.Library()


@register.simple_tag
def format_size(size):
    if size == 0:
        return '0 B'
    size_name = ('B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB')
    i = int(math.floor(math.log(size, 1024)))
    p = math.pow(1024, i)
    s = round(size / p, 3)
    return '{} {}'.format(s, size_name[i])
