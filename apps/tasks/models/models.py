from django.db import models
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth.hashers import make_password


class AdminGroups(models.Model):
	admin_id = models.IntegerField()
	group = models.CharField(max_length=128)
	group_id = models.IntegerField(blank=True, null=True)

	class Meta:
		managed = True
		db_table = 'admin_groups'


class AdminLogins(models.Model):
	admin_id = models.IntegerField(blank=True, null=True)
	last_login = models.IntegerField(blank=True, null=True)
	ip = models.CharField(max_length=16, blank=True, null=True)

	class Meta:
		managed = True
		db_table = 'admin_logins'


class Admins(models.Model):
	email = models.CharField(max_length=45)
	password = models.CharField(max_length=512)
	name = models.CharField(max_length=45)

	class Meta:
		managed = True
		db_table = 'admins'


class Billing(models.Model):
	client_id = models.IntegerField()
	email = models.CharField(max_length=45, default="demo@blomp.com")
	plan = models.CharField(max_length=45, default="basic")
	billing_cycle = models.CharField(max_length=10, null=True)
	billing_date = models.CharField(max_length=32, null=True)

	class Meta:
		managed = True
		db_table = 'billing'

	def get_client_billing(self, client_id):
		try:
			billing = Billing.objects.get(client_id=client_id)
			return billing

		except ObjectDoesNotExist:
			return None

class ClientLogins(models.Model):
	client_id = models.IntegerField()
	last_login = models.CharField(max_length=45)
	ip = models.CharField(max_length=16)

	class Meta:
		managed = True
		db_table = 'client_logins'


class ClientStorage(models.Model):
	client_id = models.IntegerField()
	username = models.CharField(max_length=128, default="None")
	password = models.CharField(max_length=256, default="password")
	ec2_access_key = models.CharField(max_length=128, default="key")
	ec2_secret_key = models.CharField(max_length=128, default="key")
	container = models.CharField(max_length=256)
	size = models.CharField(max_length=32)

	class Meta:
		managed = True
		db_table = 'client_storage'

	def getClients(self):
		try:
			clients = ClientStorage.objects.filter(size=20)
			return clients

		except ObjectDoesNotExist:
			return False

		except IndexError:
			return False

	def getStorageDetails(self, client_id):
		try:
			storage = ClientStorage.objects.get(client_id=client_id)
			return storage

		except ObjectDoesNotExist:
			return None

		except IndexError:
			return False


class Clients(models.Model):
	os_user_id = models.CharField(max_length=128, blank=True, null=True)
	email = models.CharField(max_length=128)
	password = models.CharField(max_length=512)
	name = models.CharField(max_length=512, blank=True, null=True)
	address = models.CharField(max_length=512, blank=True, null=True)
	phone = models.CharField(max_length=48, blank=True, null=True)
	company = models.CharField(max_length=256, blank=True, null=True)
	fb_user_id = models.CharField(max_length=128)
	regdate = models.CharField(max_length=45)
	status = models.IntegerField(blank=True, null=True)
	deleted = models.IntegerField(blank=True, null=True)
	confirm_number = models.CharField(max_length=16, blank=True, null=True)
	confirmed = models.IntegerField(blank=True, null=True)
	first_payment = models.IntegerField(blank=True, null=True)
	check_confirmed = models.CharField(max_length=16, blank=True, null=True)
	resetnum = models.CharField(max_length=256, blank=True, null=True)
	password_reset = models.IntegerField(blank=True, null=True)

	class Meta:
		managed = True
		db_table = 'clients'

	def get_client_details(self, client_id):
		try:
			client = Clients.objects.get(id=client_id)
			return client

		except ObjectDoesNotExist:
			return None

		except IndexError:
			return False

	def getClientName(self, client_id):
		try:
			client = Clients.objects.get(id=client_id)
			return client.name

		except ObjectDoesNotExist:
			return None

		except IndexError:
			return False
	def user_payment(self, client_id):
		try:
			payments = Clients.objects.filter(id=client_id).values()
			payment = payments[0]['first_payment']
			return payment

		except ObjectDoesNotExist:
			return None

		except IndexError:
			return None

	def checkPassword(self, client_id, password):
		try:
			client = Clients.objects.filter(id=client_id).values()
			clientPasswd = client[0]['password']
			if check_password(password, clientPasswd):
				return True
			else:
				return False

		except IndexError:
			return None

		except:
			return None

	def updatePassword(self, client_id, password):
		try:
			client = Clients.objects.get(pk=client_id)
			passwd = make_password(password)
			client.password = passwd
			client.save()
			return True

		except IndexError:
			return None

		except:
			return None

	def updateInfo(self, client_id, name, company, address, phone):
		try:
			client = Clients.objects.get(pk=client_id)
			client.name = name
			client.company = company
			client.address = address
			client.phone = phone
			client.save()
			return True

		except IndexError:
			return None

		except:
			return None

	def updateOSclientID(self, client_id, ClientID):
		try:
			client = Clients.objects.get(pk=client_id)
			client.os_user_id = ClientID
			client.save()
			return True

		except IndexError:
			return None

		except:
			return None


class ClientsPP(models.Model):
	client_id = models.IntegerField(blank=True, null=True)
	amount = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
	payment_date = models.CharField(max_length=48, blank=True, null=True)
	payment_year = models.CharField(max_length=4, blank=True)
	payment_month = models.CharField(max_length=2, blank=True)
	payment_status = models.CharField(max_length=32, blank=True, null=True)
	first_name = models.CharField(max_length=128, blank=True, null=True)
	last_name = models.CharField(max_length=128, blank=True, null=True)
	payer_email = models.CharField(max_length=128, blank=True, null=True)
	address_street = models.CharField(max_length=256, blank=True, null=True)
	address_city = models.CharField(max_length=256, blank=True, null=True)
	address_country = models.CharField(max_length=128, blank=True, null=True)
	address_state = models.CharField(max_length=128, blank=True, null=True)
	address_zip = models.CharField(max_length=32, blank=True, null=True)
	payer_id = models.CharField(max_length=128, blank=True, null=True)
	verify_sign = models.CharField(max_length=256, blank=True, null=True)

	class Meta:
		managed = True
		db_table = 'clients_pp'

	def getUserPaymentDate(self, client_id):
		try:
			payments = ClientsPP.objects.filter(id=client_id).last()
			return payments.payment_date

		except ObjectDoesNotExist:
			return False

		except IndexError:
			return False

	def getPayments(self, year, month):
		try:
			paypal = ClientsPP.objects.filter(year=year).filter(month=month)
			return paypal

		except ObjectDoesNotExist:
			return False

		except IndexError:
			return False

class Logs(models.Model):
	log_type = models.CharField(max_length=128, blank=True, null=True)
	client_email = models.CharField(max_length=256, blank=True, null=True)
	ip_address = models.CharField(max_length=16)
	description = models.TextField()
	error = models.CharField(max_length=256)
	error_code = models.IntegerField(blank=True, null=True)
	date = models.IntegerField()

	class Meta:
		managed = True
		db_table = 'logs'


class Storage(models.Model):
	project_id = models.CharField(max_length=45)
	project = models.CharField(max_length=45, blank=True, null=True)
	domain = models.CharField(max_length=45)
	status = models.IntegerField(blank=True, null=True)

	class Meta:
		managed = True
		db_table = 'storage'

class Openstack(models.Model):
	domain = models.CharField(max_length=45)
	username = models.CharField(max_length=64, blank=True, null=True)
	user_id = models.CharField(max_length=128, null=True)
	password = models.CharField(max_length=45)
	project_id = models.CharField(max_length=128, null=True)
	group = models.CharField(max_length=45, null=True)
	group_id = models.CharField(max_length=128, null=True)
	role = models.CharField(max_length=45, null=True)
	role_id = models.CharField(max_length=128, null=True)
	identity_api = models.CharField(max_length=45)
	identity_api_v2 = models.CharField(max_length=45, blank=True, null=True)
	network_api = models.CharField(max_length=45)
	telemetry_api = models.CharField(max_length=45, blank=True, null=True)
	storage_api = models.CharField(max_length=45, blank=True, null=True)

	class Meta:
		managed = True
		db_table = 'openstack'

	def details(self):
		try:
			details = Openstack.objects.get()
			return details

		except ObjectDoesNotExist:
			return False

		except IndexError:
			return False

class Plans(models.Model):
	plan = models.CharField(max_length=64, blank=True, null=True)
	price_month = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
	price_year = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
	size = models.IntegerField(blank=True, null=True)
	description = models.TextField()

	class Meta:
		managed = True
		db_table = 'plans'

	def get_plan_details(self, plan):
		try:
			plan = Plans.objects.get(plan=plan)
			return plan

		except ObjectDoesNotExist:
			return None

		except IndexError:
			return False


class Support(models.Model):
	client_id = models.IntegerField()
	instance_id = models.CharField(max_length=256)
	subject = models.CharField(max_length=512, blank=True, null=True)
	priority = models.CharField(max_length=8)
	description = models.TextField()
	date = models.IntegerField()
	closed = models.IntegerField(blank=True, null=True)

	class Meta:
		managed = True
		db_table = 'support'


class SupportAttachment(models.Model):
	client_id = models.IntegerField()
	support_id = models.IntegerField()
	attachment_url = models.CharField(max_length=512)
	date = models.IntegerField()

	class Meta:
		managed = True
		db_table = 'support_attachment'


class SupportMessages(models.Model):
	support_id = models.IntegerField()
	from_user = models.CharField(max_length=512)
	client_id = models.IntegerField()
	admin_id = models.IntegerField(blank=True, null=True)
	admin_name = models.CharField(max_length=128, blank=True, null=True)
	message = models.TextField()
	date = models.IntegerField()

	class Meta:
		managed = True
		db_table = 'support_messages'

class TempData(models.Model):
	client_id = models.IntegerField()
	password = models.CharField(max_length=512)
	plan = models.CharField(max_length=45)
	bucket = models.CharField(max_length=64)

	class Meta:
		managed = True
		db_table = 'temp_data'