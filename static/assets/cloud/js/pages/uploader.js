$(function() {

    // Defaults
    Dropzone.autoDiscover = false;

    // Removable thumbnails
    $("#dropzone_remove").dropzone({
        paramName: "myfile", // The name that will be used to transfer the file
        dictDefaultMessage: 'Drop files to upload <span>or CLICK</span>',
        addRemoveLinks: true,
        init: function() {
            this.on('success', function(){
                if (this.getQueuedFiles().length == 0 && this.getUploadingFiles().length == 0) {
                    $.jGrowl('', {
                        position: 'top-center',
                        header: 'Files uploaded successfully!',
                        theme: 'bg-primary'
                    });
                    setTimeout(function(){location.reload();}, 2000);
                }
            });
        }
    });

});
