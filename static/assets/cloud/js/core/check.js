$(document).ready(function(){

var base_url = window.location.protocol+'//'+window.location.host+'/';

Ladda.bind('.btn-ladda-spinner', {
    dataSpinnerSize: 16,
});

    $('.check-availability').click(function(){
        var form_data = {
            'forum_username': $('#id_forum_username').val(),
        };
        $.ajax({
            type: "POST",
            data: form_data,
            url: base_url + 'register/forum/check/',
            dataType: 'json',
            success: function(data){
                if (data.response){
                    swal({
                        title: "Success!",
                        text: data.message,
                        position: "top",
                        confirmButtonColor: "#EF5350",
                        type: "success",
                        animation: false
                    });
                    Ladda.stopAll();
                }
                else{
                    swal({
                        title: "Information",
                        text: data.message,
                        position: "top",
                        confirmButtonColor: "#EF5350",
                        type: "error",
                        animation: false
                    });
                    Ladda.stopAll();
                }
            },
            error: function(data){
                swal({
                    title: "Something went wrong",
                    text: "We could not process your request.",
                    confirmButtonColor: "#EF5350",
                    type: "error",
                    animation: false
                });
                Ladda.stopAll();
            }
        });
        return false;
    });



/*EOF*/
});