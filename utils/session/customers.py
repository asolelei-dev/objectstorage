from functools import wraps
from django.contrib import messages
from django.shortcuts import redirect
from utils.helpers import config
from time import time


def loginRequired(view_func):
    def _decorator(request, *args, **kwargs):
        try:
            user_id = request.session['user_id']
            remember_me = request.session['remember']
            current_time = int(time() * 1000)
            if user_id is None:
                messages.add_message(request, messages.ERROR, config.AUTH_LOGIN_ERROR)
                return redirect('/')
            elif user_id is not None:
                if remember_me == 0:
                    response = view_func(request, *args, **kwargs)
                    return response
                elif remember_me != 0:
                    if current_time > int(remember_me):
                        request.session.flush()
                        messages.add_message(request, messages.ERROR, 'Your session has expired')
                        return redirect('/')
                    else:
                        response = view_func(request, *args, **kwargs)
                        return response
        except KeyError:
            messages.add_message(request, messages.ERROR, config.AUTH_LOGIN_ERROR)
            return redirect('/')
        except ValueError:
            messages.add_message(request, messages.ERROR, config.AUTH_LOGIN_ERROR)
            return redirect('/')

    return wraps(view_func)(_decorator)
