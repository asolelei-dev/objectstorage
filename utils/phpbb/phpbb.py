
from utils.models import models
import requests
from requests.auth import HTTPBasicAuth
import time
from utils.mailer.mailer import Mailer as mailer


class Phpbb:
    def __init__(self):
        self.basic_username = 'admin'
        self.basic_password = 'JnshUiop34Dcn231As'

    def addUser(self, userId, username, email, password):
        mail = mailer()
        try:
            forumUser = models.ForumTemp()
            payload = {'username': username, 'email': email, 'password': password}
            re = requests.post('http://205.134.174.112:2089/add.php', auth=HTTPBasicAuth(self.basic_username, self.basic_password), params=payload, timeout=10)
            if re.status_code == 401:
                forumUser.insertUser(userId, username, email, password)
                mail.notify_support('Invalid API username and password [Error 401].')
            elif re.text == 'Success':
                return True
        except Exception:
            # Store the user to the temp table to retry and notify the admins and support
            forumUser.insertUser(userId, username, email, password)
            mail.notify_support('An error occured while processing the request. Check the phpbbapi service and forum server.')

    def getLogin(self, email):
        mail = mailer()
        try:
            payload = {'email': email}
            re = requests.post('http://205.134.174.112:2089/get.php', auth=HTTPBasicAuth(self.basic_username, self.basic_password), params=payload)
            if re.text == 'Error':
                mail.notify_support('Error while fetching user details from forum database. Please check the phpbbapi service and forum server.')
            else:
                # Check the date with todays date and see if it's more than 30 days
                timestamp = int(time.time())
                userLastVisit = int(re.text)
                if (timestamp - userLastVisit) > 2592000:
                    mail.notifyUser(email)
        except Exception:
            mail.notify_support('Error while fetching user details from forum database. Please check the phpbbapi service and forum server.')

    def check_username(self, username):
        mail = mailer()
        try:
            payload = {'user_name': username}
            re = requests.post('http://205.134.174.112:2089/get_username.php', auth=HTTPBasicAuth(self.basic_username, self.basic_password), params=payload)
            if re.text == 'Not Available':
                return False
            elif re.text == 'Available':
                return True

        except Exception:
            mail.notify_support('Error while fetching user details from forum database. Please check the phpbbapi service and forum server.')

    def get_username(self, email):
        try:
            payload = {'email': email}
            re = requests.post('http://205.134.174.112:2089/get_user_forumname.php', auth=HTTPBasicAuth(self.basic_username, self.basic_password), params=payload)
            return re.text

        except Exception:
            return False
