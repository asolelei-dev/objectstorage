# AUTH MESSAGES

AUTH_LOGIN_ERROR = 'Invalid email or password entered.'
AUTH_CONFIRM = 'You have to confirm your email to login.'
AUTH_PROCESSING_ERROR = 'An error occured while processing your request. Please try again or contact support.'
AUTH_NAME = 'Name can not be shorter than 5 charachters.'
AUTH_FORUM = 'Forum username can not be shorter than 5 charachters.'
AUTH_FORUM_CHAR = 'Forum username can not contain special characters. Only letters, numbers and underscores are allowed.'
AUTH_TERMS = 'You must agree to terms and conditions.'
AUTH_INVALID_EMAIL = 'Email you entered is not valid.'
AUTH_FORUM_USERNAME = 'Forum username you selected is not available. Please select another one.'
AUTH_EXISTING_USER = 'If a user with that email has not confirmed and is in our system, the message has been resent.'
AUTH_CONFIRM_EMAIL = 'To confirm your email follow the instruction in the email we sent you.'
AUTH_CONFIRM_ACCOUNT = 'To confirm your account follow the instruction in the email we sent you.'
AUTH_PASSWORD_MATCH = 'Passwords do not match. Please enter correct password.'
AUTH_ACCOUNT_CONFIRMED = 'Your account has been confirmed successfuly. Login to use the Blomp storage and Blomp software.'
AUTH_CONFIRMED_ALREADY = 'User has already been confirmed. Login to use the Blomp storage and Blomp software.'
AUTH_INVALID_CONFIRM = 'Invalid confirmation number entered.'
AUTH_RECOVER = 'We have sent you an email with instructions on how to recover your password.'
AUTH_INVALID_EMAIL = 'Email you provided is not valid.'
AUTH_PASSWORD_UPDATE = 'Your password has been updated successfuly.'
AUTH_PASSWORD_NOMATCH = 'Your passwords do not match. Please try again.'
AUTH_CONFIRM_CHANGE_PASSWORD = 'You have to confirm your account to change your password.'
AUTH_USER_NONEXISTS = 'User with this email is registered.'
AUTH_RECONFIRM = 'If you are in our system and haven’t confirmed, a message has been sent.'
AUTH_RECONFIRM_TRUE = 'This account has already been confirmed'


# DASHBOARD DATA MESSAGES
CHANGE_PASSWORD_INCCORECT = 'Incorrect current password entered'
PASSWORD_CHANGED = 'Your password has been changed.'
DATA_ERROR = 'Something went wrong. Please try again or contact support.'
ADD_INFO = 'Your information has been added to your profile.'
INVITATION_SENT = 'Invitations send successfully.'


# BILLING MESSAGES
BILLING_SUBSCRIBE = 'You must subscribe to one of the plans before proceeding.'


# USER FILES
SEGMENT_LINK = '<a href="/dashboard/files/{}/">{}/</a>'
SEGMENT_LINK_NEXT = '<a href="/dashboard/files/{}/{}">{}/</a>'
OBJECT_NAME = '<a href="/dashboard/files/{}/"><i class="icon-folder2"></i> {}</a>'
OBJECT_OPTIONS = '<div class="btn-group"><a href="/dashboard/storage/download_object?path={}" class="btn btn-xs btn-primary" data-popup="tooltip" title="" data-original-title="Download File"><i class="icon-cloud-download2"></i></a><a class="btn btn-xs btn-danger delete-object" data-path="{}" data-popup="tooltip" title="" data-original-title="Delete File"><i class="icon-trash"></i></a></div>'
