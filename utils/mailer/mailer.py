from django.core.mail import EmailMessage
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.template.loader import render_to_string


class Mailer:

    def __init__(self):
        self.sender = 'no-reply@blomp.com'
        self.bcc = 'audit@mail.blomp.com'

    def register(self, email, confirm_number, forum_username):

        message = render_to_string('emails/confirm.html', {'email': email, 'confirm_number': confirm_number, 'forum_username': forum_username})
        email = EmailMessage(
            subject='Blomp Email Verification',
            body=message,
            from_email=self.sender,
            to=[email],
            bcc=[self.bcc]

        )
        email.content_subtype = 'html'

        try:
            email.send(fail_silently=False)
            return HttpResponseRedirect(reverse('apply:success'))
        except Exception:
            return False

    def confirmed(self, email):

        message = render_to_string('emails/confirmed.html', {'email': email})
        email = EmailMessage(
            subject='Welcome to Blomp!',
            body=message,
            from_email=self.sender,
            to=[email],
            bcc=[self.bcc]

        )
        email.content_subtype = 'html'

        try:
            email.send(fail_silently=False)
            return HttpResponseRedirect(reverse('apply:success'))
        except Exception:
            return False

    def recovery(self, client_email, recoveryNumber):
        message = render_to_string('emails/forgot_password.html', {'email': client_email, 'recoveryNumber': recoveryNumber})
        email = EmailMessage(
            subject='Blomp Reset Password',
            body=message,
            from_email=self.sender,
            to=[client_email],
            bcc=[self.bcc]

        )
        email.content_subtype = 'html'

        try:
            email.send(fail_silently=False)
            return HttpResponseRedirect(reverse('apply:success'))
        except Exception:
            return False

    def password_reset(self, client_email):
        message = render_to_string('emails/reset_password.html', {'email': client_email})
        email = EmailMessage(
            subject='Blomp Reset Password',
            body=message,
            from_email=self.sender,
            to=[client_email],
            bcc=[self.bcc]

        )
        email.content_subtype = 'html'

        try:
            email.send(fail_silently=False)
            return HttpResponseRedirect(reverse('apply:success'))
        except Exception:
            return False

    def invite_email(self, client_email, invite_email, invite_number):
        message = render_to_string('emails/invited.html', {'email': client_email, 'invite_email': invite_email, 'invite_number': invite_number})
        email = EmailMessage(
            subject='Blomp Support',
            body=message,
            from_email=self.sender,
            to=[invite_email],
            bcc=[self.bcc]

        )
        email.content_subtype = 'html'
        try:
            email.send(fail_silently=False)
            return HttpResponseRedirect(reverse('apply:success'))
        except Exception:
            return False

    def space_added(self, client_email):
        message = render_to_string('emails/space_added.html', {'email': client_email})
        email = EmailMessage(
            subject='You have got more Blomp storage!',
            body=message,
            from_email=self.sender,
            to=[client_email],
            bcc=[self.bcc]

        )
        email.content_subtype = 'html'
        try:
            email.send(fail_silently=False)
            return HttpResponseRedirect(reverse('apply:success'))
        except Exception:
            return False

    def upgrade(self, client_email, plan, date_time):
        message = render_to_string('emails/space_added.html', {'email': client_email, 'plan': plan, 'date_time': date_time})
        email = EmailMessage(
            subject='Blomp Support',
            body=message,
            from_email=self.sender,
            to=[client_email],
            bcc=[self.bcc]

        )
        email.content_subtype = 'html'
        try:
            email.send(fail_silently=False)
            return HttpResponseRedirect(reverse('apply:success'))
        except Exception:
            return False

    def downgrade(self, client_email, plan, date_time):
        message = render_to_string('emails/space_added.html', {'email': client_email, 'plan': plan, 'date_time': date_time})
        email = EmailMessage(
            subject='Blomp Support',
            body=message,
            from_email=self.sender,
            to=[client_email],
            bcc=[self.bcc]

        )
        email.content_subtype = 'html'
        try:
            email.send(fail_silently=False)
            return HttpResponseRedirect(reverse('apply:success'))
        except Exception:
            return False

    def payment(self, client_email, storage, date_time):
        message = render_to_string('emails/upgraded.html', {'email': client_email, 'plan': storage, 'date_time': date_time})
        email = EmailMessage(
            subject='Blomp Support',
            body=message,
            from_email=self.sender,
            to=[client_email],
            bcc=[self.bcc]

        )
        email.content_subtype = 'html'
        try:
            email.send(fail_silently=False)
            return HttpResponseRedirect(reverse('apply:success'))
        except Exception:
            return False

    def login_soon(self, client_email):
        message = render_to_string('emails/login_soon.html', {'email': client_email})
        email = EmailMessage(
            subject='Blomp Support',
            body=message,
            from_email=self.sender,
            to=[client_email],
            bcc=[self.bcc]

        )
        email.content_subtype = 'html'
        try:
            email.send(fail_silently=False)
            return HttpResponseRedirect(reverse('apply:success'))
        except Exception:
            return False

    def account_expired(self, client_email, date_time):
        message = render_to_string('emails/account_expired.html', {'email': client_email, 'date_time': date_time})
        email = EmailMessage(
            subject='Blomp Support',
            body=message,
            from_email=self.sender,
            to=[client_email],
            bcc=[self.bcc]

        )
        email.content_subtype = 'html'
        try:
            email.send(fail_silently=False)
            return HttpResponseRedirect(reverse('apply:success'))
        except Exception:
            return False

    def notify_support(self, message):
        message = '<h5>Blomp Support Forums Notification</h5><p>An issue was detected while adding new user to Blomp Support Forums: ' + message + '</p><p>Please check if forums or phpbbapi service is running.</p><p>Thank you!</p><p><i>This is an automated message that does not need a reply.</i></p>'
        email = EmailMessage(
            subject='Blomp Support Forums Error',
            body=message,
            from_email=self.sender,
            to=['support@ai.net'],
            bcc=[self.bcc]
        )
        email.content_subtype = 'html'
        try:
            email.send(fail_silently=False)
            return HttpResponseRedirect(reverse('apply:success'))
        except Exception:
            return False

    def spam_detection(self, email):
        message = '<p>We detected an email {} that was reported as spam by stopforumspam.com. Registration for this email has been blocked</p>'.format(email)
        email = EmailMessage(
            subject='Spam Email Detected',
            body=message,
            from_email=self.sender,
            to=['support@blomp.com']
        )
        email.content_subtype = 'html'
        try:
            email.send(fail_silently=False)
            return HttpResponseRedirect(reverse('apply:success'))
        except Exception:
            return False

    def spam_service_down(self, email, status):
        message = '<p>We can not check if email {} is spam email because stopforumspam.com responded with {} status.</p>'.format(email, status)
        email = EmailMessage(
            subject='StopForumSpam Down',
            body=message,
            from_email=self.sender,
            to=['support@blomp.com']
        )
        email.content_subtype = 'html'
        try:
            email.send(fail_silently=False)
            return HttpResponseRedirect(reverse('apply:success'))
        except Exception:
            return False
