from utils.models import models
from utils.openstack.identity import Identity as identitylib
import requests


class Swift:

    def __init__(self):

        openstack = models.Openstack()
        identity = identitylib()
        self.plans = models.Plans()
        self.details = openstack.details()
        self.username = self.details.username
        self.password = self.details.password
        self.authurl = self.details.identity_api_v2
        #get token from db, extend token to be valid for 6 or 12 hours
        self.token = identity.tokenv3()
        self.session = requests.Session()
        self.session.trust_env = False

    def accountDetails(self):
        try:
            headers = {
                'X-Auth-Token': self.token,
            }

            response = self.session.get(url=self.details.storage_api + 'AUTH_' + self.details.project_id, headers=headers)

            if response.status_code == 200 or response.status_code == 204:
                return response.headers
            else:
                return False
                # log error and code
        except Exception:
            return False

    def createContainer(self, container):
        try:
            headers = {
                'Content-Length': '0',
                'X-Auth-Token': self.token,
            }

            response = self.session.put(url=self.details.storage_api + 'AUTH_' + self.details.project_id + '/' + container, headers=headers)

            if response.status_code == 202 or response.status_code == 201:
                return True
            else:
                return False
                # log error and code
        except Exception:
            return False

    def containerReadACL(self, user_id, container):
        try:
            headers = {
                'X-Auth-Token': self.token,
                'X-Container-Read': self.details.project_id + ':' + user_id + ',.rlistings',
            }

            response = self.session.put(url=self.details.storage_api + 'AUTH_' + self.details.project_id + '/' + container, headers=headers)

            if response.status_code == 202 or response.status_code == 201:
                return True
            else:
                return False
                # log error and code
        except Exception:
            return False

    def containerWriteACL(self, user_id, container):
        try:
            headers = {
                'X-Auth-Token': self.token,
                'X-Container-Write': self.details.project_id + ':' + user_id,
            }

            response = self.session.put(url=self.details.storage_api + 'AUTH_' + self.details.project_id + '/' + container, headers=headers)

            if response.status_code == 202 or response.status_code == 201:
                return True
            else:
                return False
                # log error and code
        except Exception:
            return False

    def containerShareACL(self, username, container):
        try:
            headers = {
                'X-Auth-Token': self.token,
                'X-Container-Read': username,
            }

            response = self.session.put(url=self.details.storage_api + 'AUTH_' + self.details.project_id + '/' + container, headers=headers)

            if response.status_code == 202 or response.status_code == 201:
                return True
            else:
                return False
                # log error and code
        except Exception:
            return False

    def containerSize(self, container, plansize):
        try:
            size = 1024*1024*1024 * plansize
            headers = {
                'X-Auth-Token': self.token,
                'X-Container-Meta-Quota-Bytes': str(size),
            }

            response = self.session.put(url=self.details.storage_api + 'AUTH_' + self.details.project_id + '/' + container, headers=headers)

            if response.status_code == 202 or response.status_code == 201:
                return True
            else:
                return False
                # log error and code
        except Exception:
            return False

    def containerDetails(self, container):
        try:
            headers = {
                'X-Auth-Token': self.token,
            }

            response = self.session.get(url=self.details.storage_api + 'AUTH_' + self.details.project_id + '/' + container, headers=headers)

            if response.status_code == 200 or response.status_code == 204:
                return response.headers
            else:
                return False
                # log error and code
        except Exception:
            return False

    def changeContainerSize(self, container, size):

        try:
            headers = {
                'X-Auth-Token': self.token,
            }

            response = self.session.get(url=self.details.storage_api + 'AUTH_' + self.details.project_id + '/' + container, headers=headers)

            if response.status_code == 200 or response.status_code == 204:
                headers = response.headers
                newSize = 1024 * 1024 * 1024 * int(size)
                containerQuota = headers['X-Container-Meta-Quota-Bytes']
                newQuota = int(containerQuota) + newSize
                # change size

                newHeaders = {
                    'X-Auth-Token': self.token,
                    'X-Container-Meta-Quota-Bytes': str(newQuota),
                }

                change = self.session.put(url=self.details.storage_api + 'AUTH_' + self.details.project_id + '/' + container, headers=newHeaders)

                if change.status_code == 202 or change.status_code == 201:
                    return True
                else:
                    return False
                    # log error and code
            else:
                return False
                # log error and code
        except Exception:
            return False

    def listObject(self, container):
        try:
            headers = {
                'content-type': 'application/json',
                'accept': 'application/json',
                'X-Auth-Token': self.token
            }

            response = self.session.get(url=self.details.storage_api + 'AUTH_' + self.details.project_id + '/' + container, headers=headers)

            if response.status_code == 200 or response.status_code == 204:
                return response.json()
            else:
                return False
                # log error and code
        except Exception:
            return False
