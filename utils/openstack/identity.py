from utils.models import models
import requests
import json


class Identity:

    def __init__(self):
        openstack = models.Openstack()
        self.details = openstack.details()
        self.session = requests.Session()
        self.session.trust_env = False

    def tokenv2(self):
        try:
            json_payload = {"auth": {"passwordCredentials": {"username": self.details.username, "password": self.details.password, "tenantId": self.details.project_id}}}
            headers = {'content-type': 'application/json', 'accept': 'application/json'}
            response = self.session.post(url=self.details.identity_api_v2 + '/tokens', data=json.dumps(json_payload), headers=headers)
            return response.json()
        except Exception:
            return False

    def tokenv3(self):
        try:
            json_payload = {"auth": {"identity": {"methods": ["password"], "password": {"user": {"id": self.details.user_id, "password": self.details.password}}}}}
            headers = {'content-type': 'application/json', 'accept': 'application/json'}
            response = self.session.post(url=self.details.identity_api + 'auth/tokens', data=json.dumps(json_payload), headers=headers)
            return response.headers['X-Subject-Token']
        except Exception:
            return False

    def createUser(self, token, name, email, password):
        try:
            json_payload = {"user": {"default_project_id": self.details.project_id, "description": "Blomp Storage User", "domain_id": "default", "email": email, "enabled": True, "name": name, "password": password}}
            headers = {'content-type': 'application/json', 'accept': 'application/json', 'x-auth-token': token}
            response = self.session.post(url=self.details.identity_api + 'users/', data=json.dumps(json_payload), headers=headers)
            return response.json()
        except Exception:
            return False

    def createEC2Credentials(self, token, access_key, secret_key, user_id, project_id):
        try:
            blob = {"access": access_key, "secret": secret_key}
            json_payload = {"credential": {"blob": json.dumps(blob), "project_id": project_id, "type": "ec2", "user_id": user_id}}
            headers = {'content-type': 'application/json', 'accept': 'application/json', 'x-auth-token': token}
            response = self.session.post(url=self.details.identity_api + 'credentials', data=json.dumps(json_payload), headers=headers)
            return response.json()
        except Exception:
            return response.json()

    def getEC2Credentials(self, token, user_id):
        try:
            headers = {'content-type': 'application/json', 'accept': 'application/json', 'x-auth-token': token}
            response = self.session.get(url=self.details.identity_api + 'credentials?user_id=' + user_id, headers=headers)
            return response.json()
        except Exception:
            return False

    def addGroup(self, token, userID):
        try:
            headers = {'X-Auth-Token': token}
            response = self.session.put(url=self.details.identity_api + 'groups/' + self.details.group_id + '/users/' + userID, headers=headers)
            if response.status_code == 204:
                return True
            else:
                return False
        except Exception:
            return False

    def addRole(self, token, userID):
        try:
            headers = {'X-Auth-Token': token}
            response = self.session.put(url=self.details.identity_api + 'projects/' + self.details.project_id + '/users/' + userID + '/roles/' + self.details.role_id, headers=headers)
            if response.status_code == 204:
                return True
            else:
                return False
        except Exception:
            return False

    def deleteUser(self, userID, token):
        try:
            headers = {'X-Auth-Token': token}
            response = self.session.delete(url=self.details.identity_api + 'users/' + userID, headers=headers)
            if response.status_code == 204:
                return True
            else:
                return False
        except Exception:
            return False

    def changeUserPassword(self, token, email, userID, password):
        try:
            json_payload = {"user": {"email": email, "name": email, "password": password}}
            headers = {'content-type': 'application/json', 'accept': 'application/json', 'x-auth-token': token}
            response = self.session.patch(url=self.details.identity_api + 'users/' + userID, data=json.dumps(json_payload), headers=headers)
            if response.status_code == 200:
                return True
            else:
                return False
        except Exception:
            return None

    def userDetails(self, userID, token):
        try:
            headers = {'X-Auth-Token': token}
            response = self.session.get(url=self.identity_api + 'users/' + userID, headers=headers)
            if response.status_code == 200:
                return True
            elif response.status_code == 404:
                return False
            else:
                return False
        except Exception:
            return False

    def userList(self, token):
        try:
            headers = {'X-Auth-Token': token}
            response = self.session.get(url=self.details.identity_api + 'users/', headers=headers)
            if response.status_code == 200 or response.status_code == 203:
                return response.json()
            elif response.status_code == 404:
                return False
            else:
                return False
        except Exception:
            return False
