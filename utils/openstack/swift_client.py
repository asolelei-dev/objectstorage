import swiftclient
from utils.models import models

from utils.openstack import timeutils
from utils.openstack import base

FOLDER_DELIMITER = "/"
# Swift ACL
GLOBAL_READ_ACL = ".r:*"
LIST_CONTENTS_ACL = ".rlistings"
STATUS_FALSE = 0
STATUS_TRUE = 1


class Container(base.APIDictWrapper):
    pass


class StorageObject(base.APIDictWrapper):
    def __init__(self, apidict, container_name, orig_name=None, data=None):
        super(StorageObject, self).__init__(apidict)
        self.container_name = container_name
        self.orig_name = orig_name
        self.data = data

    @property
    def id(self):
        return self.name


class PseudoFolder(base.APIDictWrapper):
    def __init__(self, apidict, container_name):
        super(PseudoFolder, self).__init__(apidict)
        self.container_name = container_name

    @property
    def id(self):
        return '%s/%s' % (self.container_name, self.name)

    @property
    def name(self):
        return self.subdir.rstrip(FOLDER_DELIMITER)

    @property
    def bytes(self):
        return None

    @property
    def content_type(self):
        return "application/pseudo-folder"


class SwiftClient:

    def __init__(self):
        openstack = models.Openstack()
        self.details = openstack.details()
        self.username = self.details.username
        self.password = self.details.password
        self.authurl = self.details.identity_api_v2
        self.swift_api = swiftclient.client.Connection(authurl='', user=self.username, key=self.password, tenant_name='storage', auth_version='3', insecure=True)

    def objectify(self, items, container_name):
        """Splits a listing of objects into their appropriate wrapper classes."""
        objects = []

        # Deal with objects and object pseudo-folders first, save subdirs for later
        for item in items:
            if item.get("subdir", None) is not None:
                object_cls = PseudoFolder
            else:
                object_cls = StorageObject

            objects.append(object_cls(item, container_name))

        return objects

    def metadata_to_header(self, metadata):
        headers = {}
        public = metadata.get('is_public')

        if public is True:
            public_container_acls = [GLOBAL_READ_ACL, LIST_CONTENTS_ACL]
            headers['x-container-read'] = ",".join(public_container_acls)
        elif public is False:
            headers['x-container-read'] = ""

        return headers

    def swift_object_exists(self, container_name, object_name):
        try:
            self.swift_api.head_object(container_name, object_name)
            return True
        except swiftclient.client.ClientException:
            return False

    def get_account(self):
        account = self.swift_api.get_account(marker=None, limit=1, prefix=None, end_marker=None, full_listing=False, headers=None)
        return account

    def container_exists(self, container_name):
        try:
            self.swift_api.head_container(container_name)
            return True
        except swiftclient.client.ClientException:
            return False

    def create_container(self, name, metadata=({})):
        if self.container_exists(name):
            return (False, "Container with this name already exists.")
        headers = self.metadata_to_header(metadata)
        try:
            self.swift_api.put_container(name, headers=headers)
            return True
        except Exception:
            return False

    def container_details(self, container_name, with_data=False):
        try:
            details = self.swift_api.head_container(container_name)
            return details
        except Exception:
            return False

    def update_container(self, container_name, metadata=({})):
        # add read and write permission in every request in header (see swift.py)
        # 'x-container-read': self.details.project_id + ':' + user_id + ',.rlistings',
        # 'x-container-write': self.details.project_id + ':' + user_id,
        try:
            self.swift_api.post_container(container_name, headers=metadata)
            return True
        except Exception:
            return False

    def delete_container(self, container_name):
        objects = self.get_objects(container_name)
        if objects:
            return (False, "Could not delete container, it contains objects.")
        try:
            self.swift_api.delete_container(container_name)
            return True
        except Exception:
            return (False, "Something went wrong. API could not process the request.")

    def object_exists(self, container_name, object_name):
        try:
            self.swift_api.head_object(container_name, object_name)
            return True
        except swiftclient.client.ClientException:
            return False

    def get_objects(self, container_name, prefix=None, marker=None, end_marker=None, limit=None):

        kwargs = dict(prefix=prefix, marker=marker, end_marker=end_marker, limit=limit, delimiter=FOLDER_DELIMITER, full_listing=True)
        headers, objects = self.swift_api.get_container(container_name, **kwargs)
        object_objs = self.objectify(objects, container_name)

        return object_objs

    def get_objects_head(self, container_name):

        headers, objects = self.swift_api.get_container(container_name)

        return headers

    def upload_object(self, container_name, object_name, object_file=None):
        headers = {}
        headers['X-Object-Meta-Orig-Filename'] = object_file.name
        size = object_file.size
        etag = self.swift_api.put_object(container_name, object_name, object_file, headers=headers)
        obj_info = {'name': object_name, 'bytes': size, 'etag': etag}
        return StorageObject(obj_info, container_name)

    def get_object(self, container_name, object_name, with_data=True):
        if with_data:
            headers, data = self.swift_api.get_object(container_name, object_name)
        else:
            data = None
            headers = self.swift_api.head_object(container_name, object_name)
        orig_name = headers.get("x-object-meta-orig-filename")
        timestamp = None
        try:
            ts_float = float(headers.get('x-timestamp'))
            timestamp = timeutils.iso8601_from_timestamp(ts_float)
        except Exception:
            pass
        obj_info = {
            'name': object_name,
            'bytes': headers.get('content-length'),
            'content_type': headers.get('content-type'),
            'etag': headers.get('etag'),
            'timestamp': timestamp,
        }
        return StorageObject(obj_info, container_name, orig_name=orig_name, data=data)

    def delete_object(self, container_name, object_name):
        try:
            self.swift_api.delete_object(container_name, object_name)
            return True
        except Exception:
            # Log error
            return True

    def create_pseudo_folder(self, container_name, pseudo_folder_name):
        headers = {}
        try:
            self.swift_api.put_object(container_name, pseudo_folder_name, None, headers=headers)
            return True

        except Exception:
            return False

    def copy_object(self, orig_container_name, orig_object_name,
                          new_container_name, new_object_name):
        if self.swift_object_exists(new_container_name, new_object_name):
            return False

        headers = {"X-Copy-From": FOLDER_DELIMITER.join([orig_container_name,
                                                         orig_object_name])}
        self.swift_api.put_object(new_container_name,
                                             new_object_name,
                                             None,
                                             headers=headers)

        return True
