from django.db import models
from django.core.exceptions import ObjectDoesNotExist


class ClientStorage(models.Model):
    client_id = models.IntegerField()
    username = models.CharField(max_length=128, default="None")
    password = models.CharField(max_length=256, default="password")
    ec2_access_key = models.CharField(max_length=128, default="key")
    ec2_secret_key = models.CharField(max_length=128, default="key")
    container = models.CharField(max_length=256)
    size = models.CharField(max_length=10)

    class Meta:
        managed = False
        db_table = 'client_storage'


class Clients(models.Model):
    os_user_id = models.CharField(max_length=128, blank=True, null=True)
    email = models.CharField(max_length=128)
    password = models.CharField(max_length=512)
    name = models.CharField(max_length=512, blank=True, null=True)
    address = models.CharField(max_length=512, blank=True, null=True)
    phone = models.CharField(max_length=48, blank=True, null=True)
    company = models.CharField(max_length=256, blank=True, null=True)
    fb_user_id = models.CharField(max_length=128)
    regdate = models.CharField(max_length=45)
    status = models.IntegerField(blank=True, null=True)
    deleted = models.IntegerField(blank=True, null=True)
    confirmed = models.IntegerField(blank=True, null=True)
    first_payment = models.IntegerField(blank=True, null=True)
    check_confirmed = models.CharField(max_length=16, blank=True, null=True)
    resetnum = models.CharField(max_length=256, blank=True, null=True)
    password_reset = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'clients'


class Storage(models.Model):
    project_id = models.CharField(max_length=45)
    project = models.CharField(max_length=45, blank=True, null=True)
    domain = models.CharField(max_length=45)
    status = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'storage'


class Openstack(models.Model):
    domain = models.CharField(max_length=45)
    username = models.CharField(max_length=64, blank=True, null=True)
    user_id = models.CharField(max_length=128, null=True)
    password = models.CharField(max_length=45)
    project_id = models.CharField(max_length=128, null=True)
    group = models.CharField(max_length=45, null=True)
    group_id = models.CharField(max_length=128, null=True)
    role = models.CharField(max_length=45, null=True)
    role_id = models.CharField(max_length=128, null=True)
    identity_api = models.CharField(max_length=45)
    identity_api_v2 = models.CharField(max_length=45, blank=True, null=True)
    network_api = models.CharField(max_length=45)
    telemetry_api = models.CharField(max_length=45, blank=True, null=True)
    storage_api = models.CharField(max_length=45, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'openstack'

    def details(self):
        try:
            details = Openstack.objects.get(id=1)
            return details

        except ObjectDoesNotExist:
            return False

        except IndexError:
            return False


class Plans(models.Model):
    plan = models.CharField(max_length=64, blank=True, null=True)
    price_month = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    price_year = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    size = models.IntegerField(blank=True, null=True)
    description = models.TextField()

    class Meta:
        managed = False
        db_table = 'plans'

    def get_plan_details(self, plan):
        try:
            plan = Plans.objects.get(plan=plan)
            return plan

        except ObjectDoesNotExist:
            return None

        except IndexError:
            return False


class ForumTemp(models.Model):
    user_id = models.IntegerField()
    username = models.CharField(max_length=256)
    email = models.CharField(max_length=256)
    password = models.CharField(max_length=512)

    class Meta:
        managed = False
        db_table = 'forum_temp'

    def insertUser(self, userId, username, email, password):
        try:
            forumUser = ForumTemp(user_id=userId, username=username, email=email, password=password)
            forumUser.save()
            return True

        except ObjectDoesNotExist:
            return None
        except IndexError:
            return None

    def deleteUser(self, email):
        try:
            ForumTemp.objects.filter(email=email).delete()

        except ObjectDoesNotExist:
            return None
        except IndexError:
            return None


class NotificationRead(models.Model):
    notification_id = models.IntegerField()
    client_id = models.IntegerField()
    status = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'notification_read'

    def get_ids(self):
        try:
            notifications = NotificationRead.objects.all().values('id')
            return notifications
        except Exception:
            return False


class Notifications(models.Model):
    client_id = models.IntegerField(blank=True, null=True)
    title = models.CharField(max_length=512)
    message = models.TextField()
    date_time = models.CharField(max_length=64)
    status = models.CharField(max_length=32, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'notifications'

    def get_ids(self):
        try:
            notifications = Notifications.objects.all().values('id')
            return notifications
        except Exception:
            return False

    def get_title(self, notification_id):
        try:
            title = Notifications.objects.get(pk=notification_id).values('title')
            return title
        except Exception:
            return False
