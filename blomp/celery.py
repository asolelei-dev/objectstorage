from __future__ import absolute_import, unicode_literals
import os
import django
from django.conf import settings
from celery import Celery
import sys
from os.path import join

# set the default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'blomp.settings')
sys.path.append(join(settings.BASE_DIR, 'apps'))
django.setup()

app = Celery('blomp')

# Using a string here means the worker doesn't have to serialize
# the configuration object to child processes.
# - namespace='CELERY' means all celery-related configuration keys
#   should have a `CELERY_` prefix.
app.config_from_object('django.conf:settings')

# Load task modules from all registered Django app configs.
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)


@app.task(bind=True)
def debug_task(self):
    print('Request: {0!r}'.format(self.request))